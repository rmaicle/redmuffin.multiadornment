﻿## redmuffin.MultiAdornment 
### Extension for Visual Studio 2012 & 2013 places visual adornments around regions and preprocessor directives and highlights special code comments in C#, VB.Net and javaScript even HTML and Razor comments.

>       // x deleted
>       // - small
>       // + big
>       // ! important
>       // ? question

### Whitespace around symbols/keywords

>       //  ! important
>       //?question
>       //+ big
>       //  -small

### User defined symbols/keywords like "Hack, Todo, Attention, Warning"

>       // HACK: this is clearly a hack...

### HTML comments

>       <!-- + even HTML comments work in all supported files -->

### Razor comments

>       @* ? finally, Razor comments *@

### Single line 'multi-line comments'

>       public string PropertyName { get; /* ! single line 'multi-line comment' format */ set; }

### XML doc comments

>       /// <summary>
>       /// ! Make sure this is tripple checked!
>       /// </summary>

### Multiple comments in one line

>		start <!-- ! HTML --> <a href="#">something</a> @* ? Razor *@ lorem ipsum /* multi-line comment */ <div>something else</div> // Note: and the end

---

[Release Notes](https://bitbucket.org/michaelvolz/redmuffin.multiadornment/wiki/Release%20Notes)

This Visual Studio extension is **open source** 

** Any feedback or suggestions are welcome! 