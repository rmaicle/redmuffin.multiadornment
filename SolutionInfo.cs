using System.Reflection;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
using redmuffin.MultiAdornment;

[assembly: AssemblyCompany( "redmuffin" )]
[assembly: AssemblyProduct( "redmuffin.MultiAdornment" )]
[assembly: AssemblyCopyright( "(c) 2013 by Michael A. Volz (mvolz@redmuffin.de)" )]
[assembly: AssemblyTrademark( "---" )]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion( Constants.VersionNumber )]
[assembly: AssemblyFileVersion( Constants.VersionNumber )]
