﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Microsoft.VisualStudio.Text.Classification;
using NLog;
using redmuffin.MultiAdornment.Core;
using redmuffin.MultiAdornment.OptionsPage;

namespace redmuffin.MultiAdornment.Adornments
{
    public class LiveRegionOptions : RegionOptions
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public LiveRegionOptions( IEditorFormatMap editorFormatMap )
            : base( Cache.Enabled, Cache.ContentTypes, Cache.BorderThickness, Cache.GetRegionBrushes( editorFormatMap ) ) {
            Contract.Requires( editorFormatMap != null );

        }

        internal static class Cache
        {
            static Cache() {
                RefreshCachedValues();
            }

            internal static bool Enabled { get; private set; }
            internal static HashSet<string> ContentTypes { get; private set; }
            internal static int BorderThickness { get; private set; }

            private static Dictionary<string, List<Brush>> Brushes { get; set; }

            internal static void RefreshCachedValues() {
                Enabled = Options.GetValue( o => o.RegionHighlighting );
                BorderThickness = Options.GetValue( o => o.BorderThickness );

                string contentTypes = Options.GetValue( o => o.SupportedRegionContenTypes );
                ContentTypes = contentTypes.ConvertToLoweredTrimmedHashSet();

                // ! Lazy refresh
                Brushes = null;
            }

            internal static Dictionary<string, List<Brush>> GetRegionBrushes( IEditorFormatMap editorFormatMap ) {
                Contract.Requires( editorFormatMap != null );

                if ( Brushes != null ) return Brushes;

                Brushes = RegionAdornmentManager.Keywords.ToDictionary(
                    keyWord => keyWord,
                    keyWord => new List<Brush>
                               {
                                   CreateBrushFromColorOption( "MA " + keyWord.Replace( " ", "" ).Replace( "\t", "" ), editorFormatMap ),
                                   CreateBrushFromColorOption( "MA " + keyWord.Replace( " ", "" ).Replace( "\t", "" ) + " Border", editorFormatMap )
                               } );

                return Brushes;
            }

            private static Brush CreateBrushFromColorOption( string editorFormatDefinitionName, IEditorFormatMap editorFormatMap ) {
                Contract.Requires( !String.IsNullOrEmpty( editorFormatDefinitionName ) );

                if ( KeywordHasNoBorder( editorFormatDefinitionName ) )
                    return null;

                Logger.Debug( "CreateBrushFromColorOption for " + editorFormatDefinitionName );

                ResourceDictionary dictionary = editorFormatMap.GetProperties( editorFormatDefinitionName );
                Contract.Assert( dictionary != null );

                var brush = (SolidColorBrush)dictionary[EditorFormatDefinition.BackgroundBrushId];
                if ( brush != null ) brush.Freeze();

                return brush;
            }

            private static bool KeywordHasNoBorder( string editorFormatDefinitionName ) {
                return editorFormatDefinitionName == "MA #else Border";
            }
        }

        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
