﻿using System.Collections.Generic;
using System.Windows.Media;

namespace redmuffin.MultiAdornment.Adornments
{
    public class RegionOptions : IRegionOptions
    {
        private readonly HashSet<string> _splitTrimmedAndLoweredContentTypes;

        #region Implementation of IRegionOptions

        public bool Enabled { get; private set; }
        public int BorderThickness { get; private set; }
        public Dictionary<string, List<Brush>> Brushes { get; set; }

        public bool ContentTypeAllowed( string currentContentType ) {
            return _splitTrimmedAndLoweredContentTypes.Contains( currentContentType );
        }

        #endregion

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        protected RegionOptions( bool enabled, HashSet<string> splitTrimmedAndLoweredContentTypes, int borderThickness, Dictionary<string, List<Brush>> brushes ) {
            _splitTrimmedAndLoweredContentTypes = splitTrimmedAndLoweredContentTypes;
            Enabled = enabled;
            BorderThickness = borderThickness;
            Brushes = brushes;
        }
    }
}
