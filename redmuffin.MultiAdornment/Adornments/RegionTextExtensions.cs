using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.VisualStudio.Text.Formatting;

namespace redmuffin.MultiAdornment.Adornments
{
    public static class RegionTextExtensions
    {
        public static HashSet<T> ToHashSet<T>( this IEnumerable<T> source ) {
            return new HashSet<T>( source );
        }


        public static string ToLowerTrimmedWhiteSpace( this ITextViewLine line ) {
            Contract.Requires( line != null );

            return line.Extent.GetText().ToLowerTrimmedWhiteSpace();
        }

        [UsedImplicitly]
        public static string ToLowerTrimmedWhiteSpace( this string text ) {
            Contract.Requires( text != null );

            return text.ToLower().Trim().Trim( Convert.ToChar( "\t" ) ).Trim();
        }

        public static bool IsStartKeyword( this string text ) {
            Contract.Requires( !String.IsNullOrEmpty( text ) );

            return RegionAdornmentManager.StartKeywords.Any( text.StartsWith );
        }

        public static bool IsCollapsedRegion( this string text ) {
            Contract.Requires( !String.IsNullOrEmpty( text ) );

            // ReSharper disable StringLiteralTypo
            return ( text.StartsWith( "#region" ) && text.Contains( "#endregion" ) )
                   || ( text.StartsWith( "#region" ) && text.Contains( "#end region" ) );
            // ReSharper restore StringLiteralTypo
        }

        public static bool IsEndKeyword( this string text ) {
            Contract.Requires( !String.IsNullOrEmpty( text ) );

            return RegionAdornmentManager.EndKeywords.Any( text.StartsWith );
        }

        public static bool IsMiddleKeyword( this string text ) {
            Contract.Requires( !String.IsNullOrEmpty( text ) );

            return RegionAdornmentManager.MiddleKeywords.Any( text.StartsWith );
        }


        public static string GetKeyword( this string text ) {
            Contract.Requires( !String.IsNullOrEmpty( text ) );

            return RegionAdornmentManager.Keywords.FirstOrDefault( text.StartsWith );
        }
    }
}
