﻿using System.Diagnostics.Contracts;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.VisualStudio.Text.Editor;

namespace redmuffin.MultiAdornment.Adornments.ViewPort
{
    /// <summary>
    ///     Adornment class that draws a square box in the top right hand corner of the viewport
    /// </summary>
    internal class ViewportAdornment1
    {
        private readonly IAdornmentLayer _adornmentLayer;
        private readonly Image _image;
        private readonly IWpfTextView _view;

        /// <summary>
        ///     Creates a square image and attaches an event handler to the layout changed event that adds the the square in the
        ///     upper right-hand corner of the TextView via the adornment layer
        /// </summary>
        /// <param name="view">The <see cref="IWpfTextView" /> upon which the adornment will be drawn</param>
        public ViewportAdornment1( IWpfTextView view ) {
            Contract.Requires( view != null );

            _view = view;

            // create the drawingImage
            _image = new Image();
            _image.Source = DrawingImage();

            //Grab a reference to the adornment layer that this adornment should be added to
            _adornmentLayer = view.GetAdornmentLayer( "ViewportAdornment1" );

            _view.ViewportHeightChanged += delegate { OnSizeChange(); };
            _view.ViewportWidthChanged += delegate { OnSizeChange(); };
        }

        private DrawingImage DrawingImage() {
            Brush brush = new SolidColorBrush( Colors.BlueViolet );
            brush.Freeze();
            Brush penBrush = new SolidColorBrush( Colors.Red );
            penBrush.Freeze();
            var pen = new Pen( penBrush, 0.5 );
            pen.Freeze();

            //draw a square with the created brush and pen
            var r = new Rect( 0, 0, 30, 30 );
            Geometry g = new RectangleGeometry( r );
            var drawing = new GeometryDrawing( brush, pen, g );
            drawing.Freeze();

            var drawingImage = new DrawingImage( drawing );
            drawingImage.Freeze();

            return drawingImage;
        }

        public void OnSizeChange() {
            //clear the adornment layer of previous adornments
            _adornmentLayer.RemoveAllAdornments();

            //Place the image in the top right hand corner of the Viewport
            Canvas.SetLeft( _image, _view.ViewportRight - 60 );
            Canvas.SetTop( _image, _view.ViewportTop + 30 );

            //add the image to the adornment layer and make it relative to the viewport
            _adornmentLayer.AddAdornment( AdornmentPositioningBehavior.ViewportRelative, null, null, _image, null );
        }
    }
}
