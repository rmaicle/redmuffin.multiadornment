﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;

namespace redmuffin.MultiAdornment.Adornments.ViewPort
{

    #region Adornment Factory

    /// <summary>
    ///     Establishes an <see cref="IAdornmentLayer" /> to place the adornment on and exports the
    ///     <see cref="IWpfTextViewCreationListener" /> that instantiates the adornment on the event of a
    ///     <see cref="IWpfTextView" />'s creation
    /// </summary>
    [ContentType( "text" )]
    //[Export( typeof ( IWpfTextViewCreationListener ) )]
    [TextViewRole( PredefinedTextViewRoles.Document )]
    internal sealed class PurpleBoxAdornmentFactory : IWpfTextViewCreationListener
    {
        /// <summary>
        ///     Defines the adornment layer for the scarlet adornment. This layer is ordered after the selection layer in the
        ///     Z-order
        /// </summary>
        [Name( "ViewportAdornment1" )]
        [Export( typeof ( AdornmentLayerDefinition ) )]
        [Order( After = PredefinedAdornmentLayers.Caret )]
        public AdornmentLayerDefinition EditorAdornmentLayer = null;

        /// <summary>
        ///     Instantiates a ViewportAdornment1 manager when a textView is created.
        /// </summary>
        /// <param name="textView">The <see cref="IWpfTextView" /> upon which the adornment should be placed</param>
        public void TextViewCreated( IWpfTextView textView ) {
            // ReSharper disable once ObjectCreationAsStatement
            new ViewportAdornment1( textView );
        }
    }

    #endregion //- Adornment Factory
}
