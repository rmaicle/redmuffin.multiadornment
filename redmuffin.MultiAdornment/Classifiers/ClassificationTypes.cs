﻿using System.ComponentModel.Composition;
using System.Windows.Media;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;
using redmuffin.MultiAdornment.OptionsPage;

namespace redmuffin.MultiAdornment.Classifiers
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class ClassificationTypes
    {
        [Export( typeof ( ClassificationTypeDefinition ) )]
        [Name( StandardStyle.ClassName )]
        internal static ClassificationTypeDefinition DefaultClassifierType = null;

        [Export(typeof(ClassificationTypeDefinition))]
        [Name(DetailsUserStyle1.ClassName)]
        internal static ClassificationTypeDefinition DetailsUser1ClassifierType = null;

        [Export(typeof(ClassificationTypeDefinition))]
        [Name(DetailsUserStyle2.ClassName)]
        internal static ClassificationTypeDefinition DetailsUser2ClassifierType = null;

        [Export(typeof(ClassificationTypeDefinition))]
        [Name(DetailsUserStyle3.ClassName)]
        internal static ClassificationTypeDefinition DetailsUser3ClassifierType = null;

        [Export(typeof(ClassificationTypeDefinition))]
        [Name(DoxygenStyle.ClassName)]
        internal static ClassificationTypeDefinition DoxygenClassifierType = null;

        [Export(typeof(ClassificationTypeDefinition))]
        [Name( NoteStyle.ClassName)]
        internal static ClassificationTypeDefinition NoteClassifierType = null;

        [Export(typeof(ClassificationTypeDefinition))]
        [Name(DoubleQuoteStyle.ClassName)]
        internal static ClassificationTypeDefinition DoubleQuoteClassifierType = null;

        [Export( typeof ( ClassificationTypeDefinition ) )]
        [Name( ImportantStyle.ClassName )]
        internal static ClassificationTypeDefinition ImportantClassifierType = null;

        [Export( typeof ( ClassificationTypeDefinition ) )]
        [Name( QuestionStyle.ClassName )]
        internal static ClassificationTypeDefinition QuestionClassifierType = null;

        [Export( typeof ( ClassificationTypeDefinition ) )]
        [Name( SmallStyle.ClassName )]
        internal static ClassificationTypeDefinition SmallClassifierType = null;

        [Export( typeof ( ClassificationTypeDefinition ) )]
        [Name( BigStyle.ClassName )]
        internal static ClassificationTypeDefinition BigClassifierType = null;

        [Export( typeof ( ClassificationTypeDefinition ) )]
        [Name( DeletedStyle.ClassName )]
        internal static ClassificationTypeDefinition DeletedClassifierType = null;

        [Export( typeof ( ClassificationTypeDefinition ) )]
        [Name( UserStyle1.ClassName )]
        internal static ClassificationTypeDefinition User1ClassifierType = null;

        [Export( typeof ( ClassificationTypeDefinition ) )]
        [Name( UserStyle2.ClassName )]
        internal static ClassificationTypeDefinition User2ClassifierType = null;

        [Export( typeof ( ClassificationTypeDefinition ) )]
        [Name( UserStyle3.ClassName )]
        internal static ClassificationTypeDefinition User3ClassifierType = null;
    }

    [Export( typeof ( EditorFormatDefinition ) )]
    [ClassificationType( ClassificationTypeNames = ClassName )]
    [Name( ColorName )]
    [UserVisible( true )] //this should be visible to the end user
    [Order( After = Priority.High )] //set the priority to be after the default classifiers
    internal sealed class UserStyle1 : ClassificationFormatDefinition
    {
        public const string ClassName = "User" + "Comment1";
        private const string ColorName = "MA User Comment1";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.BlueViolet;

        public UserStyle1() {
            FontRenderingSize = Options.GetValue( o => o.UserComment1FontSize );

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue( o => o.ItalicComments );
            //if ( Options.GetValue( o => o.UnderlineComments ) )
            //    TextDecorations = System.Windows.TextDecorations.Underline;
            FontTypeface = new Typeface( Options.GetValue( o => o.CommentsFontFamilyName ) );
        }
    }

    [Export( typeof ( EditorFormatDefinition ) )]
    [ClassificationType( ClassificationTypeNames = ClassName )]
    [Name( ColorName )]
    [UserVisible( true )] //this should be visible to the end user
    [Order( After = Priority.High )] //set the priority to be after the default classifiers
    internal sealed class UserStyle2 : ClassificationFormatDefinition
    {
        public const string ClassName = "User" + "Comment2";
        private const string ColorName = "MA User Comment2";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.DodgerBlue;

        public UserStyle2() {
            FontRenderingSize = Options.GetValue( o => o.UserComment2FontSize );

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue( o => o.ItalicComments );
            //if ( Options.GetValue( o => o.UnderlineComments ) )
            //    TextDecorations = System.Windows.TextDecorations.Underline;
            FontTypeface = new Typeface( Options.GetValue( o => o.CommentsFontFamilyName ) );
        }
    }

    [Export( typeof ( EditorFormatDefinition ) )]
    [ClassificationType( ClassificationTypeNames = ClassName )]
    [Name( ColorName )]
    [UserVisible( true )] //this should be visible to the end user
    [Order( After = Priority.High )] //set the priority to be after the default classifiers
    internal sealed class UserStyle3 : ClassificationFormatDefinition
    {
        public const string ClassName = "User" + "Comment3";
        private const string ColorName = "MA User Comment3";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.Silver;

        public UserStyle3() {
            FontRenderingSize = Options.GetValue( o => o.UserComment3FontSize );

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue( o => o.ItalicComments );
            //~ if ( Options.GetValue( o => o.UnderlineComments ) )
            //~    TextDecorations = System.Windows.TextDecorations.Underline;
            FontTypeface = new Typeface( Options.GetValue( o => o.CommentsFontFamilyName ) );
        }
    }

    [Export( typeof ( EditorFormatDefinition ) )]
    [ClassificationType( ClassificationTypeNames = ClassName )]
    [Name( ColorName )]
    [UserVisible( false )] //this should NOT be visible to the end user
    [Order( After = Priority.Default )] //set the priority to be after the default classifiers
    internal sealed class StandardStyle : ClassificationFormatDefinition
    {
        public const string ClassName = "Standard" + "Comment";
        private const string ColorName = "MA Standard Comment";
        private const string ColorDisplayName = ColorName;

        public StandardStyle() {
            // only set font properties, but no colors
            FontRenderingSize = Options.GetValue( o => o.DefaultCommentFontSize );

            DisplayName = ColorDisplayName;
            //IsItalic = Options.GetValue( o => o.ItalicComments );
            FontTypeface = new Typeface( Options.GetValue( o => o.CommentsFontFamilyName ) );
        }
    }

    // rmaicle
    // -------------------------------------------------------------------------

    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = ClassName)]
    [Name(ColorName)]
    [UserVisible(true)] //this should be visible to the end user
    [Order(After = Priority.Default)] //set the priority to be after the default classifiers
    internal sealed class DetailsUserStyle1 : ClassificationFormatDefinition
    {
        public const string ClassName = "DetailsUser" + "Comment1";
        private const string ColorName = "MA Details User Comment1";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.CornflowerBlue;

        public DetailsUserStyle1()
        {
            FontRenderingSize = Options.GetValue(o => o.DefaultCommentFontSize);

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue(o => o.ItalicComments);
            FontTypeface = new Typeface(Options.GetValue(o => o.CommentsFontFamilyName));
        }
    }

    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = ClassName)]
    [Name(ColorName)]
    [UserVisible(true)] //this should be visible to the end user
    [Order(After = Priority.Default)] //set the priority to be after the default classifiers
    internal sealed class DetailsUserStyle2 : ClassificationFormatDefinition
    {
        public const string ClassName = "DetailsUser" + "Comment2";
        private const string ColorName = "MA Details User Comment2";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.CornflowerBlue;

        public DetailsUserStyle2()
        {
            FontRenderingSize = Options.GetValue(o => o.DefaultCommentFontSize);

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue(o => o.ItalicComments);
            FontTypeface = new Typeface(Options.GetValue(o => o.CommentsFontFamilyName));
        }
    }

    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = ClassName)]
    [Name(ColorName)]
    [UserVisible(true)] //this should be visible to the end user
    [Order(After = Priority.Default)] //set the priority to be after the default classifiers
    internal sealed class DetailsUserStyle3 : ClassificationFormatDefinition
    {
        public const string ClassName = "DetailsUser" + "Comment3";
        private const string ColorName = "MA Details User Comment3";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.CornflowerBlue;

        public DetailsUserStyle3()
        {
            FontRenderingSize = Options.GetValue(o => o.DefaultCommentFontSize);

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue(o => o.ItalicComments);
            FontTypeface = new Typeface(Options.GetValue(o => o.CommentsFontFamilyName));
        }
    }

    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = ClassName)]
    [Name(ColorName)]
    [UserVisible(true)] //this should be visible to the end user
    [Order(After = Priority.Default)] //set the priority to be after the default classifiers
    internal sealed class DoxygenStyle : ClassificationFormatDefinition
    {
        public const string ClassName = "Doxygen" + "Comment";
        private const string ColorName = "MA Doxygen Comment";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.CornflowerBlue;

        public DoxygenStyle()
        {
            FontRenderingSize = Options.GetValue(o => o.DefaultCommentFontSize);

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue(o => o.ItalicComments);
            FontTypeface = new Typeface(Options.GetValue(o => o.CommentsFontFamilyName));
        }
    }

    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = ClassName)]
    [Name(ColorName)]
    [UserVisible(true)] //this should be visible to the end user
    [Order(After = Priority.Default)] //set the priority to be after the default classifiers
    internal sealed class NoteStyle : ClassificationFormatDefinition
    {
        public const string ClassName = "Note" + "Comment";
        private const string ColorName = "MA Note Comment";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.CornflowerBlue;

        public NoteStyle()
        {
            FontRenderingSize = Options.GetValue(o => o.DefaultCommentFontSize);

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue(o => o.ItalicComments);
            FontTypeface = new Typeface(Options.GetValue(o => o.CommentsFontFamilyName));
        }
    }

    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = ClassName)]
    [Name(ColorName)]
    [UserVisible(true)] //this should be visible to the end user
    [Order(After = Priority.Default)] //set the priority to be after the default classifiers
    internal sealed class DoubleQuoteStyle : ClassificationFormatDefinition
    {
        public const string ClassName = "DoubleQuote" + "Comment";
        private const string ColorName = "MA Double Quote Comment";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.CornflowerBlue;

        public DoubleQuoteStyle()
        {
            FontRenderingSize = Options.GetValue(o => o.DefaultCommentFontSize);

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue(o => o.ItalicComments);
            FontTypeface = new Typeface(Options.GetValue(o => o.CommentsFontFamilyName));
        }
    }

    // -------------------------------------------------------------------------
    // rmaicle - end

    [Export( typeof ( EditorFormatDefinition ) )]
    [ClassificationType( ClassificationTypeNames = ClassName )]
    [Name( ColorName )]
    [UserVisible( true )] //this should be visible to the end user
    [Order( After = Priority.Default )] //set the priority to be after the default classifiers
    internal sealed class ImportantStyle : ClassificationFormatDefinition
    {
        public const string ClassName = "Important" + "Comment";
        private const string ColorName = "MA Important Comment";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.CornflowerBlue;

        public ImportantStyle() {
            FontRenderingSize = Options.GetValue( o => o.ImportantCommentFontSize );

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue( o => o.ItalicComments );
            FontTypeface = new Typeface( Options.GetValue( o => o.CommentsFontFamilyName ) );
        }
    }

    [Export( typeof ( EditorFormatDefinition ) )]
    [ClassificationType( ClassificationTypeNames = ClassName )]
    [Name( ColorName )]
    [UserVisible( true )] //this should be visible to the end user
    [Order( After = Priority.Default )] //set the priority to be after the default classifiers
    internal sealed class QuestionStyle : ClassificationFormatDefinition
    {
        public const string ClassName = "Question" + "Comment";
        private const string ColorName = "MA Question Comment";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.Chocolate;

        public QuestionStyle() {
            FontRenderingSize = Options.GetValue( o => o.QuestionCommentFontSize );

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue( o => o.ItalicComments );
            FontTypeface = new Typeface( Options.GetValue( o => o.CommentsFontFamilyName ) );
        }
    }

    [Export( typeof ( EditorFormatDefinition ) )]
    [ClassificationType( ClassificationTypeNames = ClassName )]
    [Name( ColorName )]
    [UserVisible( true )] //this should be visible to the end user
    [Order( After = Priority.Default )] //set the priority to be after the default classifiers
    internal sealed class SmallStyle : ClassificationFormatDefinition
    {
        public const string ClassName = "Small" + "Comment";
        private const string ColorName = "MA Small Comment";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.DarkSeaGreen;

        public SmallStyle() {
            FontRenderingSize = Options.GetValue( o => o.SmallCommentFontSize );

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue( o => o.ItalicComments );
            FontTypeface = new Typeface( Options.GetValue( o => o.CommentsFontFamilyName ) );
        }
    }

    [Export( typeof ( EditorFormatDefinition ) )]
    [ClassificationType( ClassificationTypeNames = ClassName )]
    [Name( ColorName )]
    [UserVisible( true )] //this should be visible to the end user
    [Order( After = Priority.Default )] //set the priority to be after the default classifiers
    internal sealed class BigStyle : ClassificationFormatDefinition
    {
        public const string ClassName = "Big" + "Comment";
        private const string ColorName = "MA Big Comment";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.CadetBlue;

        public BigStyle() {
            FontRenderingSize = Options.GetValue( o => o.BigCommentFontSize );

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            IsItalic = Options.GetValue( o => o.ItalicComments );
            FontTypeface = new Typeface( Options.GetValue( o => o.CommentsFontFamilyName ) );
        }
    }

    [Export( typeof ( EditorFormatDefinition ) )]
    [ClassificationType( ClassificationTypeNames = ClassName )]
    [Name( ColorName )]
    [UserVisible( true )] //this should be visible to the end user
    [Order( After = Priority.Default )] //set the priority to be after the default classifiers
    internal sealed class DeletedStyle : ClassificationFormatDefinition
    {
        public const string ClassName = "Deleted" + "Comment";
        private const string ColorName = "MA Deleted Comment";
        private const string ColorDisplayName = ColorName;
        private static readonly Color? _backgroundColorFormat = null;
        private static readonly Color _foregroundColorFormat = Colors.LightGray;

        public DeletedStyle() {
            FontRenderingSize = Options.GetValue( o => o.DeletedCommentFontSize );

            DisplayName = ColorDisplayName;
            ForegroundColor = _foregroundColorFormat;
            BackgroundColor = _backgroundColorFormat;
            //~ IsItalic = Options.GetValue( o => o.ItalicComments );
            FontTypeface = new Typeface( Options.GetValue( o => o.CommentsFontFamilyName ) );
        }
    }
}
