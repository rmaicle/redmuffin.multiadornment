using System.Diagnostics.Contracts;

namespace redmuffin.MultiAdornment.Classifiers
{
    public class Comment
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Comment" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="openingTag">The opening tag.</param>
        /// <param name="closingTag">The closing tag.</param>
        public Comment( string name, string openingTag = "", string closingTag = "" ) {
            OpeningTag = openingTag;
            ClosingTag = closingTag;
            Name = name;
        }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name { get; private set; }

        /// <summary>
        ///     Gets the opening tag.
        /// </summary>
        /// <value>
        ///     The opening tag.
        /// </value>
        public string OpeningTag { get; private set; }

        /// <summary>
        ///     Gets the closing tag. Can be empty.
        /// </summary>
        /// <value>
        ///     The closing tag.
        /// </value>
        public string ClosingTag { get; private set; }

        /// <summary>
        ///     Standard comment with 2 slashes
        /// </summary>
        /// <example>// comment</example>
        public static Comment StandardComment {
            get { return new Comment( "StandardComment" ); }
        }

        /// <summary>
        ///     Multi-line comment wit slash and asterisk
        /// </summary>
        /// <example>/* comment */</example>

        public static Comment MultiLineComment {
            get { return new Comment( "MultiLineComment" ); }
        }

        /// <summary>
        ///     XML doc comment with 3 slashes
        /// </summary>
        /// <example>/// comment</example>
        public static Comment XmlComment {
            get { return new Comment( "XmlComment" ); }
        }

        /// <summary>
        ///     HTML comment
        /// </summary>
        /// <example><!-- a HTML comment --></example>
        public static Comment HtmlComment {
            get { return new Comment( "HtmlComment" ); }
        }

        /// <summary>
        ///     Razor comment
        /// </summary>
        /// <example>@* a Razor comment *@</example>
        public static Comment RazorComment {
            get { return new Comment( "RazorComment" ); }
        }

        /// <summary>
        ///     Visual Basic comment with one high comma
        /// </summary>
        /// <example>' comment</example>
        public static Comment VisualBasicComment {
            get { return new Comment( "VisualBasicComment" ); }
        }

        /// <summary>
        ///     Visual Basic XML doc comment with three high commas
        /// </summary>
        /// <example>''' comment</example>
        public static Comment VisualBasicXmlComment {
            get { return new Comment( "VisualBasicXmlComment" ); }
        }

        #region Overridden methods

        /// <summary>
        ///     Returns the name of the comment.
        /// </summary>
        /// <returns>
        ///     The name of the comment.
        /// </returns>
        public override string ToString() {
            return Name;
        }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals( object obj ) {
            // ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
            if ( obj is Comment ) return Equals( (Comment)obj );
            return false;
        }

        public bool Equals( Comment comment ) {
            Contract.Requires( comment != null );

            return ( Name == comment.Name );
        }

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode() {
            return Name.GetHashCode();
        }

        public static bool operator ==( Comment lhs, Comment rhs ) {
            Contract.Requires( rhs != null );

            // ReSharper disable once PossibleNullReferenceException
            return lhs.Equals( rhs );
        }

        public static bool operator !=( Comment lhs, Comment rhs ) {
            Contract.Requires( rhs != null );

            // ReSharper disable once PossibleNullReferenceException
            return !( lhs.Equals( rhs ) );
        }

        #endregion
    }
}
