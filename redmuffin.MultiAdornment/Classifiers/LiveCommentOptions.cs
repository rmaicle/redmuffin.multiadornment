﻿using System;
using System.Collections.Generic;
using redmuffin.MultiAdornment.Core;
using redmuffin.MultiAdornment.OptionsPage;

namespace redmuffin.MultiAdornment.Classifiers
{
    internal class LiveCommentOptions : CommentOptions
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CommentOptions" /> class using cached values from the options page.
        /// </summary>
        public LiveCommentOptions( Comment comment )
            : base(
                Cache.Enabled( comment ),
                Cache.ContentTypes( comment ),
                Cache.ProcessedUserSymbols1,
                Cache.ProcessedUserSymbols2,
                Cache.ProcessedUserSymbols3 ) {}

        /// <summary>
        ///     This subclass holds cached (static) items to speed up performance.
        /// </summary>
        public static class Cache
        {
            /// <summary>
            ///     Initializes the <see cref="Cache" /> class.
            /// </summary>
            static Cache() {
                RefreshCachedValues();
            }

            internal static bool StandardCommentHighlightingEnabled { get; set; }
            internal static bool RazorHighlightingEnabled { get; set; }
            internal static bool HtmlHighlightingEnabled { get; set; }

            internal static HashSet<string> ProcessedContentTypes { get; set; }
            internal static HashSet<string> ProcessedUserSymbols1 { get; set; }
            internal static HashSet<string> ProcessedUserSymbols2 { get; set; }
            internal static HashSet<string> ProcessedUserSymbols3 { get; set; }

            /// <summary>
            ///     Refreshes all the cached option values.
            /// </summary>
            internal static void RefreshCachedValues() {
                StandardCommentHighlightingEnabled = Options.GetValue( o => o.StandardCommentHighlighting );
                RazorHighlightingEnabled = Options.GetValue( o => o.RazorCommentHighlighting );
                HtmlHighlightingEnabled = Options.GetValue( o => o.HtmlCommentHighlighting );

                ProcessedUserSymbols1 = ( (string)Options.GetValue( o => o.UserComment1Symbols ) ).ConvertToLoweredTrimmedHashSet();
                ProcessedUserSymbols2 = ( (string)Options.GetValue( o => o.UserComment2Symbols ) ).ConvertToLoweredTrimmedHashSet();
                ProcessedUserSymbols3 = ( (string)Options.GetValue( o => o.UserComment3Symbols ) ).ConvertToLoweredTrimmedHashSet();
                ProcessedContentTypes = ( (string)Options.GetValue( o => o.SupportedCommentContenTypes ) ).ConvertToLoweredTrimmedHashSet();
            }

            /// <summary>
            ///     Returns the appropriate content types for this <see cref="Comment" />.
            /// </summary>
            /// <param name="comment">The comment.</param>
            /// <returns>A trimmed and lowered list of valid content types.</returns>
            /// <exception cref="System.NotSupportedException">This <see cref="Comment" /> type is not supported</exception>
            internal static HashSet<string> ContentTypes( Comment comment ) {
                // ReSharper disable StringLiteralTypo
                if ( comment == Comment.StandardComment || comment == Comment.MultiLineComment )
                    return ProcessedContentTypes;

                if ( comment == Comment.VisualBasicComment )
                    // TODO: integrate into options page
                    return new HashSet<string>( new[] { "basic", "razorvisualbasic" } );

                if ( comment == Comment.HtmlComment )
                    // TODO: integrate into options page
                    return new HashSet<string>( new[] { "html", "htmlx", "plaintext", "razorcsharp", "razorvisualbasic", "xml" } );

                if ( comment == Comment.RazorComment )
                    // TODO: integrate into options page
                    return new HashSet<string>( new[] { "razorvisualbasic", "razorcsharp" } );
                // ReSharper restore StringLiteralTypo

                throw new NotSupportedException( string.Format( "This Comment type is not supported: {0}", comment ) );
            }

            /// <summary>
            ///     Verifies that the highlighting for this <see cref="Comment" /> is enabled.
            /// </summary>
            /// <param name="comment">The comment.</param>
            /// <returns>True if the highlighting for this comment is enabled.</returns>
            /// <exception cref="System.NotSupportedException">This <see cref="Comment" /> type is not supported</exception>
            internal static bool Enabled( Comment comment ) {
                if ( comment == Comment.StandardComment || comment == Comment.MultiLineComment || comment == Comment.VisualBasicComment )
                    return StandardCommentHighlightingEnabled;

                if ( comment == Comment.HtmlComment )
                    return HtmlHighlightingEnabled;

                if ( comment == Comment.RazorComment )
                    return RazorHighlightingEnabled;

                throw new NotSupportedException( "This CommentType is not supported: " + comment );
            }
        }
    }
}
