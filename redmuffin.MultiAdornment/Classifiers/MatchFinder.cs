﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using NLog;

namespace redmuffin.MultiAdornment.Classifiers
{
    public class MatchFinder
    {
        private readonly IMatchable _commentClassifier;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MatchFinder" /> class.
        /// </summary>
        /// <param name="commentClassifier">The commentClassifier.</param>
        public MatchFinder( IMatchable commentClassifier ) {
            _commentClassifier = commentClassifier;
        }

        /// <summary>
        ///     Finds all comments.
        /// </summary>
        /// <returns>A list of <see cref="ClassificationSpan" />.</returns>
        public IList<ClassificationSpan> FindSpans() {
            List<Tuple<string, Group>> matches = FindMatches().ToList();

            if ( !matches.Any() ) {
                Logger.Trace( "No match found!" );
                return new List<ClassificationSpan>();
            }

            return matches.Select( match => new ClassificationSpan( CreateSnapshotSpan( match ), GetType( match ) ) ).ToList();
        }


        private IEnumerable<Tuple<string, Group>> FindMatches() {
            string text = _commentClassifier.Span.GetText();
            Regex regEx = _commentClassifier.RegEx;

            Match match = regEx.Match( text );

            while ( match.Success ) {
                Group key = match.Groups["Key"];
                Group value = match.Groups["Value"];

                Logger.Trace( "Match found:\t..." + value.Value.Replace( Environment.NewLine, "" ) + "..." );

                // TODO: replace Tuple with custom class!
                if ( !StandardCommentFoundInQuotes( text, value ) )
                    yield return new Tuple<string, Group>( key.Value, value );

                match = regEx.Match( text, match.Index + match.Length );
            }
        }

        private SnapshotSpan CreateSnapshotSpan( Tuple<string, Group> match ) {
            Contract.Requires( match != null );

            return new SnapshotSpan( _commentClassifier.Span.Snapshot, _commentClassifier.Span.Start + match.Item2.Index, match.Item2.Length );
        }


        private IClassificationType GetType( Tuple<string, Group> match ) {
            Contract.Requires( match != null );

            string key = match.Item1.ToLower();

            // Prioritize comments with keywords

            if (_commentClassifier.Options.UserComment1Symbols.Contains(key))
                return _commentClassifier.Registry.GetClassificationType(UserStyle1.ClassName);

            if (_commentClassifier.Options.UserComment2Symbols.Contains(key))
                return _commentClassifier.Registry.GetClassificationType(UserStyle2.ClassName);

            if (_commentClassifier.Options.UserComment3Symbols.Contains(key))
                return _commentClassifier.Registry.GetClassificationType(UserStyle3.ClassName);

            //' NOTE: Character restrictions
            //'
            //' Cannot use * because multi-line comments is interpreted as a special style.
            //' Cannot use ! because it interferes with Doxygen comments.
            //? Cannot use x because parsing // x is treated as a special style.
            //?   This is because the regex allows space(s) after //.
            //' Cannot use - because // ---- is commonly used.

            if (key == ">") return _commentClassifier.Registry.GetClassificationType(DetailsUserStyle1.ClassName);
            if (key == ":") return _commentClassifier.Registry.GetClassificationType(DetailsUserStyle2.ClassName);
            if (key == ".") return _commentClassifier.Registry.GetClassificationType(DetailsUserStyle3.ClassName);

            if ( key == "!" ) return _commentClassifier.Registry.GetClassificationType( DoxygenStyle.ClassName );
            if ( key == "'" ) return _commentClassifier.Registry.GetClassificationType( NoteStyle.ClassName );
            if ( key == "\"" ) return _commentClassifier.Registry.GetClassificationType( DoubleQuoteStyle.ClassName );

            if ( key == "*" ) return _commentClassifier.Registry.GetClassificationType( ImportantStyle.ClassName );
            if ( key == "?" ) return _commentClassifier.Registry.GetClassificationType( QuestionStyle.ClassName );
            if ( key == "+" ) return _commentClassifier.Registry.GetClassificationType( BigStyle.ClassName );
            if ( key == "_" ) return _commentClassifier.Registry.GetClassificationType( SmallStyle.ClassName );
            if ( key == "~" ) return _commentClassifier.Registry.GetClassificationType( DeletedStyle.ClassName );

            return _commentClassifier.Registry.GetClassificationType( StandardStyle.ClassName );
        }

        private bool StandardCommentFoundInQuotes( string line, Group matchedComment ) {
            if ( _commentClassifier.Comment == Comment.StandardComment || _commentClassifier.Comment == Comment.VisualBasicComment ) {
                // Hack: this is very naive and should be rethought! It works most of the time though...
                int firstComment = line.IndexOf( @"""", StringComparison.Ordinal );
                bool quotesBeforeComment = ( firstComment > 0 && firstComment < matchedComment.Index );
                bool quotesInsideComment = matchedComment.Value.Contains( @"""" );
                bool commentIsInQuotes = quotesBeforeComment && quotesInsideComment;

                if ( commentIsInQuotes ) {
                    Logger.Info( "Match was skipped beacause comment seems to be surrounded by quotes: " + line );
                    return true;
                }
            }

            return false;
        }

        #region Logger

        private static Logger _logger;


        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
