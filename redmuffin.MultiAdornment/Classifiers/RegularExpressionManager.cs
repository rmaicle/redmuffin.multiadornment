﻿using System;
using System.Diagnostics.Contracts;
using System.Text.RegularExpressions;
using NLog;

namespace redmuffin.MultiAdornment.Classifiers
{
    //. TODO: Special comments should not be treated in multi-line comments
    //. /*
    //. //* <----- this is highlighted
    //. */

    public class RegularExpressionManager
    {
        private const string VisualBasicStart = @"(?<Value>('''|')\s*(?<Key>";
        private const string VisualBasicEnd = @")?[^']*$)";

        private const string StandardStart = @"(^|[^:])(?<Value>///?\s*(?<Key>";
        private const string StandardEnd = @")?.*)";

        //. TODO: Do not allow space(s) between // and symbol.
        //. The following regex is tested in http://regexstorm.net/tester
        //. but does not work here.
        //. @"(^|^\s*)((?<value>//)(?<key>(\s*(hack|note|bug|kludge))|[?+~'#|\""|\.])?.*)",

        private const string HtmlStart = @"(?<Value><!--\s*(?<Key>";
        private const string HtmlEnd = @")?[^>]*-->)";

        private const string RazorStart = @"(?<Value>@\*\s*(?<Key>";
        private const string RazorEnd = @")?[^@]*\*@)";

        private const string MultiLineStart = @"(?<Value>/\*(?<Key>";
        private const string MultiLineEnd = @")?[^/]*\*/)";

        private static string _keywords;

        private readonly Regex _htmlRegEx;
        private readonly Regex _multiLineRegEx;
        private readonly Regex _razorRegEx;
        private readonly Regex _standardRegEx;
        private readonly string _symbolsAndKeywords = @"(" + _keywords + @")|" + @"[?+_~':!>|\*|\.|\""]";
        private readonly Regex _visualBasicRegEx;

        public RegularExpressionManager( string keywords ) {
            Contract.Requires( !String.IsNullOrEmpty( keywords ) );

            _keywords = keywords;

            _visualBasicRegEx = new Regex(
                VisualBasicStart + _symbolsAndKeywords + VisualBasicEnd,
                RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.ExplicitCapture );

            _standardRegEx = new Regex(
                StandardStart + _symbolsAndKeywords + StandardEnd,
                RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.ExplicitCapture );

            _htmlRegEx = new Regex(
                HtmlStart + _symbolsAndKeywords + HtmlEnd,
                RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.ExplicitCapture );

            _razorRegEx = new Regex(
                RazorStart + _symbolsAndKeywords + RazorEnd,
                RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.ExplicitCapture );

            _multiLineRegEx = new Regex(
                MultiLineStart + _symbolsAndKeywords + MultiLineEnd,
                RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.ExplicitCapture );
        }


        public Regex CreateRegEx( Comment comment ) {
            string text = string.Format( "{0} RegEx created:\t", comment );

            if ( comment == Comment.VisualBasicComment ) {
                Logger.Trace( text + _visualBasicRegEx );
                return _visualBasicRegEx;
            }

            if ( comment == Comment.StandardComment ) {
                Logger.Trace( text + _standardRegEx );
                return _standardRegEx;
            }

            if ( comment == Comment.HtmlComment ) {
                Logger.Trace( text + _htmlRegEx );
                return _htmlRegEx;
            }

            if ( comment == Comment.RazorComment ) {
                Logger.Trace( text + _razorRegEx );
                return _razorRegEx;
            }

            if ( comment == Comment.MultiLineComment ) {
                Logger.Trace( text + _multiLineRegEx );
                return _multiLineRegEx;
            }

            throw new NotImplementedException( "Comment not supported: " + comment );
        }

        #region Logger

        private static Logger _logger;


        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
