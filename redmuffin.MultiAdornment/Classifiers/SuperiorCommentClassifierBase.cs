﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using NLog;
using redmuffin.MultiAdornment.Core;

namespace redmuffin.MultiAdornment.Classifiers
{
    public class SuperiorCommentClassifierBase : IClassifier, IMatchable
    {
        private static readonly IList<ClassificationSpan> _emptyClassificationSpans = new List<ClassificationSpan>();

        private long _debugElapsedTime;

        /// <summary>
        ///     Gets the elapsed time for GetClassificationSpans. This is useful for performance tests.
        /// </summary>
        internal long DebugElapsedTime {
            get { return _debugElapsedTime; }
        }

        public SnapshotSpan Span { get; private set; }
        public Regex RegEx { get; private set; }
        public IClassificationTypeRegistryService Registry { get; protected set; }
        public ICommentOptions Options { get; protected set; }
        public Comment Comment { get; protected set; }

        #region Implementation of IClassifier

        /// <summary>
        ///     Gets all the <see cref="T:Microsoft.VisualStudio.Text.Classification.ClassificationSpan" /> objects that overlap
        ///     the given range of text.
        /// </summary>
        /// <returns>
        ///     A list of <see cref="T:Microsoft.VisualStudio.Text.Classification.ClassificationSpan" /> objects that intersect
        ///     with the given range.
        /// </returns>
        /// <param name="span">The snapshot span.</param>
        public IList<ClassificationSpan> GetClassificationSpans( SnapshotSpan span ) {
            Contract.Assert( span.Length >= 0 );
            Contract.Assert( span.Snapshot != null );
            Contract.Assert( span.Snapshot.ContentType != null );
            Contract.Assert( span.Snapshot.ContentType.TypeName != null );

            DebugTimer debugTimer = DebugTimer.Start();
            Span = span;

            try {
                if ( !Options.CommentHighlightingEnabled ) {
                    Logger.Trace( Comment + " module disabled!" );
                    return _emptyClassificationSpans;
                }

                if ( !Options.ContentTypeAllowed( Span.Snapshot.ContentType.TypeName ) ) {
                    Logger.Trace( string.Format( "{0} contenttype not allowed: {1}", Comment, Span.Snapshot.ContentType.TypeName ) );
                    return _emptyClassificationSpans;
                }

                RegEx = new RegularExpressionManager( Options.AllUserKeywordsRegExFragment ).CreateRegEx( Comment );

                Contract.Assert( RegEx != null );

                return new MatchFinder( this ).FindSpans();
            }
            catch ( Exception ex ) {
                Logger.Error( ex.ToString() );
                throw new InvalidOperationException( "'GetClassificationSpans' did not complete successfully!", ex );
            }
            finally {
                _debugElapsedTime = debugTimer.Stop( Comment.Name, Span.GetText() );
            }
        }

        public event EventHandler<ClassificationChangedEventArgs> ClassificationChanged {
            add { }
            remove { }
        }

        #endregion

        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
