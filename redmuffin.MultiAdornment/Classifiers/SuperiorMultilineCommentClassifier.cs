﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace redmuffin.MultiAdornment.Classifiers
{
    [ContentType( "text" )]
    [Export( typeof ( IClassifierProvider ) )]
    internal class SuperiorMultilineCommentClassifierProvider : IClassifierProvider
    {
        [Import] // ReSharper disable once RedundantDefaultFieldInitializer
        internal IClassificationTypeRegistryService ClassificationRegistry = null;

        public IClassifier GetClassifier( ITextBuffer textBuffer ) {
            return textBuffer.Properties.GetOrCreateSingletonProperty(
                () => new SuperiorMultilineCommentClassifier( ClassificationRegistry, new LiveCommentOptions( Comment.MultiLineComment ) ) );
        }
    }

    public class SuperiorMultilineCommentClassifier : SuperiorCommentClassifierBase
    {
        public SuperiorMultilineCommentClassifier( IClassificationTypeRegistryService registry, ICommentOptions options ) {
            Registry = registry;
            Options = options;
            // TODO: put this into the options class
            Comment = Comment.MultiLineComment;
        }
    }
}
