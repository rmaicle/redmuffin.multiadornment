﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace redmuffin.MultiAdornment.Classifiers
{
    [ContentType( "text" )]
    [Export( typeof ( IClassifierProvider ) )]
    internal class SuperiorRazorCommentClassifierProvider : IClassifierProvider
    {
        [Import] // ReSharper disable once RedundantDefaultFieldInitializer
        internal IClassificationTypeRegistryService ClassificationRegistry = null;

        public IClassifier GetClassifier( ITextBuffer textBuffer ) {
            return textBuffer.Properties.GetOrCreateSingletonProperty(
                () => new SuperiorRazorCommentClassifier( ClassificationRegistry, new LiveCommentOptions( Comment.RazorComment ) ) );
        }
    }

    public class SuperiorRazorCommentClassifier : SuperiorCommentClassifierBase
    {
        public SuperiorRazorCommentClassifier( IClassificationTypeRegistryService registry, ICommentOptions options ) {
            Registry = registry;
            Options = options;
            Comment = Comment.RazorComment;
        }
    }
}
