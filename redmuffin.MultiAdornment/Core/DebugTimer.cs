using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using NLog;
using redmuffin.MultiAdornment.Classifiers;

namespace redmuffin.MultiAdornment.Core
{
    public class DebugTimer
    {
        private const int TextPreviewLength = 60;
        private const int ThresholdInNanoseconds = 2500;
        private readonly int _threshold;
        private Stopwatch _stopwatch;

        /// <summary>
        ///     Prevents a default instance of the <see cref="DebugTimer" /> class from being created.
        /// </summary>
        /// <param name="threshold"></param>
        private DebugTimer( int threshold ) {
            _threshold = threshold;
        }

        /// <summary>
        ///     Generates a new timer, starts the clock and returns the instance.
        /// </summary>
        /// <param name="threshold">The threshold of Ticks above a debug message should be generated.</param>
        /// <returns>
        ///     newly generated and started Timer instance.
        /// </returns>
        public static DebugTimer Start( int threshold = ThresholdInNanoseconds ) {
            var timer = new DebugTimer( threshold );
            timer.DebugCalculateTimeStart();
            return timer;
        }

        /// <summary>
        ///     Stops the Timer and writes debug messages if necessary.
        /// </summary>
        /// <param name="name">The <see cref="Comment" />.</param>
        /// <param name="lineText">The text for the current line.</param>
        public long Stop( string name, string lineText = "" ) {
            Contract.Requires( !String.IsNullOrEmpty( name ) );

            return DebugCalculateTimeStop( lineText, name );
        }

        [Conditional( "DEBUG" )]
        private void DebugCalculateTimeStart() {
            _stopwatch = Stopwatch.StartNew();
        }

        private long DebugCalculateTimeStop( string text, string name ) {
            Contract.Requires( !String.IsNullOrEmpty( text ) );
            Contract.Requires( name != null );

#if DEBUG
            long ticks = _stopwatch.Elapsed.Ticks;
            _stopwatch.Reset();
            _stopwatch = null;

            if ( ticks > _threshold )
                Logger.Info( string.Format( "{0} took {1}s\t{2}", name, FormattedTicks( ticks ), TextPreview( text ) ) );

            return ticks;
#else
            return 0;
#endif
        }

        private static string FormattedTicks( long ticks ) {
            Contract.Requires( ticks >= 0 );
            Contract.Requires( ticks <= 3155378975999999999 );

            return new DateTime( ticks ).ToString( "s.fffffff" );
        }

        private static string TextPreview( string text ) {
            Contract.Requires( !String.IsNullOrEmpty( text ) );

            return
                text.Substring( 0, text.Length >= TextPreviewLength ? TextPreviewLength : text.Length )
                    .Replace( @"\r\n", "" )
                    .Replace( Environment.NewLine, "" );
        }

        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
