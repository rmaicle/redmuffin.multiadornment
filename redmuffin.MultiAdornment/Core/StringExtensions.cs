﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using redmuffin.MultiAdornment.Adornments;

namespace redmuffin.MultiAdornment.Core
{
    public static class StringExtensions
    {
        public static string JoinValues( this IEnumerable<string> collection ) {
            Contract.Requires( collection != null );

            return String.Join( ", ", collection.Select( x => x ) );
        }

        public static HashSet<string> ConvertToLoweredTrimmedHashSet( this string symbolString ) {
            Contract.Requires( symbolString != null );

            string[] list = symbolString.Split( ",".ToCharArray() );
            return list.Select( i => i.Trim().ToLower() ).ToHashSet();
        }
    }
}