﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics.Contracts;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using NLog;
using redmuffin.MultiAdornment.Adornments;

namespace redmuffin.MultiAdornment.Core.TextViewCreationListeners
{
    /// <summary>
    ///     Establishes an <see cref="IAdornmentLayer" /> to place the adornment on and exports the
    ///     <see cref="IWpfTextViewCreationListener" /> that instantiates the adornment on the event of a
    ///     <see cref="IWpfTextView" />'s creation
    /// </summary>
    [ContentType( "text" )]
    [Export( typeof ( IWpfTextViewCreationListener ) )]
    [TextViewRole( PredefinedTextViewRoles.Document )]
    internal sealed class CreateRegionsListener : IWpfTextViewCreationListener, IDisposable
    {
        /// <summary>
        ///     Defines the adornment layer for the adornment. This layer is ordered after the selection layer in the Z-order
        /// </summary>
        [Name( RegionAdornmentManager.RegionAdornmentLayerName )]
        [Export( typeof ( AdornmentLayerDefinition ) )]
        [Order( After = PredefinedAdornmentLayers.Selection, Before = PredefinedAdornmentLayers.Text )]
        internal AdornmentLayerDefinition EditorAdornmentLayer = null;

        /// <summary>
        ///     The format map service
        /// </summary>
        [Import] // ReSharper disable once RedundantDefaultFieldInitializer
        internal IEditorFormatMapService FormatMapService = null;

        private RegionAdornmentManager _regionAdornmentManager;

        /// <summary>
        ///     Instantiates a RegionAdornmentManager manager when a textView is created.
        /// </summary>
        /// <param name="textView">The <see cref="IWpfTextView" /> upon which the adornment should be placed</param>
        public void TextViewCreated( IWpfTextView textView ) {
            Contract.Assert( textView != null );
            Contract.Assert( FormatMapService != null );

            Logger.Trace( "CreateRegionsListener" );

            IRegionOptions regionOptions = new LiveRegionOptions( FormatMapService.GetEditorFormatMap( textView ) );

            if ( !regionOptions.Enabled )
                return;

            string currentContentType = textView.TextDataModel.ContentType.TypeName.ToLower();
            if ( !regionOptions.ContentTypeAllowed( currentContentType ) ) {
                Logger.Trace( "REGION contenttype not allowed: " + currentContentType );
                return;
            }

            _regionAdornmentManager = new RegionAdornmentManager( textView, FormatMapService, regionOptions );
        }

        #region IDisposable Members

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose() {
            if ( _regionAdornmentManager != null ) _regionAdornmentManager.Dispose();
        }

        #endregion

        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
