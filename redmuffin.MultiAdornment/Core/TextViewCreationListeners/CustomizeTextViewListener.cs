﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics.Contracts;
using System.Windows;
using System.Windows.Media;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using NLog;

namespace redmuffin.MultiAdornment.Core.TextViewCreationListeners
{
    [ContentType( "text" )]
    [Export( typeof ( IWpfTextViewCreationListener ) )]
    [TextViewRole( PredefinedTextViewRoles.Document )]
    public class CustomizeTextViewListener : IWpfTextViewCreationListener
    {
        private static bool _alreadyDone;

        /// <summary>
        ///     The format map service
        /// </summary>
        [Import] // ReSharper disable once RedundantDefaultFieldInitializer
        public IEditorFormatMapService FormatMapService = null;

        #region Implementation of IWpfTextViewCreationListener

        /// <summary>
        ///     Called when a text view having matching roles is created over a text data model having a matching content type.
        /// </summary>
        /// <param name="textView">The newly created text view.</param>
        public void TextViewCreated( IWpfTextView textView ) {
            Contract.Assert( textView != null );
            Contract.Assert( FormatMapService != null );

            Logger.Trace( "CustomizeTextViewListener" );

            if ( _alreadyDone ) return;

            ReplaceSomeVsDefaultColors( textView );
        }

        #endregion

        private void ReplaceSomeVsDefaultColors( IWpfTextView textView ) {
            Contract.Requires( textView != null );
            Contract.Assert( FormatMapService != null );

            if ( _alreadyDone ) return;

            IEditorFormatMap formatMap = FormatMapService.GetEditorFormatMap( textView );

            formatMap.BeginBatchUpdate();

            #region not useful anymore, should be deleted later

            //            CustomizeColors( formatMap, "MA Default Comment" );

            //            ResourceDictionary dc = formatMap.GetProperties( "MA Default Comment" );
            //            var dcfb = (SolidColorBrush)dc[EditorFormatDefinition.ForegroundBrushId];
            //            var dcbb = (SolidColorBrush)dc[EditorFormatDefinition.BackgroundBrushId];

            //            CustomizeColors( formatMap, "Comment", dcfb, dcbb );
            //            CustomizeColors( formatMap, "Html Comment", dcfb, dcbb );
            //            CustomizeColors( formatMap, "XML Comment", dcfb, dcbb );
            //            CustomizeColors( formatMap, "XAML Comment", dcfb, dcbb );
            //            CustomizeColors( formatMap, "VB XML Comment", dcfb, dcbb );

            #endregion

            ResourceDictionary r = formatMap.GetProperties( "MA #Region" );
            var rfb = (SolidColorBrush)r[EditorFormatDefinition.ForegroundBrushId];
            CustomizeColors( formatMap, "Collapsible Text (Collapsed)", rfb );

            formatMap.EndBatchUpdate();

            #region Unused code example

            //            ResourceDictionary regularCaretProperties = formatMap.GetProperties( "Caret" );
            //            ResourceDictionary overwriteCaretProperties = formatMap.GetProperties( "Overwrite Caret" );
            //            ResourceDictionary indicatorMargin = formatMap.GetProperties( "Indicator Margin" );
            //            ResourceDictionary visibleWhitespace = formatMap.GetProperties( "Visible Whitespace" );
            //            ResourceDictionary selectedText = formatMap.GetProperties( "Selected Text" );
            //            ResourceDictionary inactiveSelectedText = formatMap.GetProperties( "Inactive Selected Text" );
            //
            //            formatMap.BeginBatchUpdate();
            //
            //            regularCaretProperties[EditorFormatDefinition.ForegroundBrushId] = Brushes.Magenta;
            //            formatMap.SetProperties( "Caret", regularCaretProperties );
            //
            //            overwriteCaretProperties[EditorFormatDefinition.ForegroundBrushId] = Brushes.Turquoise;
            //            formatMap.SetProperties( "Overwrite Caret", overwriteCaretProperties );
            //
            //            indicatorMargin[EditorFormatDefinition.ForegroundColorId] = Colors.LightGreen;
            //            formatMap.SetProperties( "Indicator Margin", indicatorMargin );
            //
            //            visibleWhitespace[EditorFormatDefinition.BackgroundColorId] = Colors.Red;
            //            formatMap.SetProperties( "Visible Whitespace", visibleWhitespace );
            //
            //            selectedText[EditorFormatDefinition.BackgroundBrushId] = Brushes.LightPink;
            //            formatMap.SetProperties( "Selected Text", selectedText );
            //
            //            inactiveSelectedText[EditorFormatDefinition.BackgroundBrushId] = Brushes.DeepPink;
            //            formatMap.SetProperties( "Inactive Selected Text", inactiveSelectedText );
            //
            //            formatMap.EndBatchUpdate();

            #endregion

            // do this only once!
            _alreadyDone = true;

            Logger.Trace( "Default text colors modified" );
        }

        private void CustomizeColors( IEditorFormatMap formatMap, string key, SolidColorBrush foreground = null, SolidColorBrush background = null ) {
            Contract.Requires( formatMap != null );
            Contract.Requires( !String.IsNullOrEmpty( key ) );

            ResourceDictionary properties = formatMap.GetProperties( key );
            properties[EditorFormatDefinition.ForegroundBrushId] = foreground;
            properties[EditorFormatDefinition.BackgroundBrushId] = background;
            formatMap.SetProperties( key, properties );
        }

        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
