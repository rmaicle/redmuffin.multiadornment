﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Media;
using EnvDTE;
using JetBrains.Annotations;
using NLog;
using redmuffin.MultiAdornment.Adornments;
using redmuffin.MultiAdornment.Classifiers;
using redmuffin.MultiAdornment.Core;

namespace redmuffin.MultiAdornment.OptionsPage
{
    /// <summary>
    ///     Some helper methods for the options page.
    /// </summary>
    public static class Options
    {
        internal const string PageName = @"Multi Adornment";
        internal const string DefaultCategory = "General";

        /// <summary>
        ///     Gets the TextEditor font size from the "Fonts and Colors" settings page.
        /// </summary>
        /// <returns>FontSize</returns>
        [UsedImplicitly]
        public static double GetFontSize() {
            Properties properties = MultiAdornmentPackage.DTE.Properties["FontsAndColors", "TextEditor"];
            dynamic fontSize = properties.Item( "FontSize" ).Value;
            return fontSize;
        }

        /// <summary>
        ///     Gets the specified value from the options page.
        /// </summary>
        /// <param name="expression">The property expression:  o => o.PropertyName".</param>
        /// <param name="category">The category.</param>
        /// <returns>The value as dynamic.</returns>
        public static dynamic GetValue( Expression<Func<OptionsPageGeneral, dynamic>> expression, string category = DefaultCategory ) {
            Contract.Requires( expression != null );

            return GetValue( GetName.Of( expression ), category );
        }

        private static dynamic GetValue( string name, string category = DefaultCategory ) {
            Contract.Requires( name != null );

            Properties properties = MultiAdornmentPackage.DTE.Properties[PageName, category];
            Property property = properties.Item( name );
            return property.Value;
        }

        /// <summary>
        ///     Verifies that the given font exists.
        /// </summary>
        /// <param name="fontFamilyName">The font family name to look for.</param>
        /// <returns>True if the font was found.</returns>
        public static bool FontFamilyExists( string fontFamilyName ) {
            Contract.Requires( fontFamilyName != null );

            return Fonts.SystemFontFamilies.Any( family => family.FamilyNames.Any( x => x.Value == fontFamilyName ) );
        }

        public static void RefreshCachedValues() {
            Logger.Debug( "Refreshing cached values" );

            // ? Maybe make this event driven
            LiveCommentOptions.Cache.RefreshCachedValues();
            LiveRegionOptions.Cache.RefreshCachedValues();
        }

        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion
    }
}
