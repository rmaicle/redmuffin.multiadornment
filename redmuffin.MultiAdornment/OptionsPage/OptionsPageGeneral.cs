using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.VisualStudio.Shell;

namespace redmuffin.MultiAdornment.OptionsPage
{
    /// <summary>
    ///     Extends a standard dialog functionality for implementing ToolsOptions pages, with support for the Visual Studio
    ///     automation model, Windows Forms, and state persistence through the Visual Studio settings mechanism.
    /// </summary>
    [Guid( Constants.GuidOptionsPageGeneral )]
    [ComVisible( true )]
    public class OptionsPageGeneral : DialogPage
    {
        #region Fields

        private const int DefaulttFontSize = 12;
        private const string Restart = " (*restart required)";
        private const string Document = " (*document re-opening required)";

        private double? _bigCommentFontSize = 15;
        private int _borderThickness = 1;
        private bool _standardCommentHighlighting = true;
        private string _commentsFontFamilyName = "Segoe UI";
        private double? _defaultCommentFontSize = DefaulttFontSize;
        private double? _deletedCommentFontSize = DefaulttFontSize;
        private bool _htmlCommentHighlighting = true;
        //private double? _noteCommentFontSize = DefaulttFontSize;
        //private double? _asteriskCommentFontSize = DefaulttFontSize;
        private double? _importantCommentFontSize = DefaulttFontSize;
        private bool _italicComments = true;
        private double? _questionCommentFontSize = DefaulttFontSize;
        private bool _razorCommentHighlighting = true;
        private bool _regionHighlighting = true;
        private double? _smallCommentFontSize = 10;
        private string _supportedCommentContenTypes = "plaintext, C/C++, CSharp, Basic, XML, HTML, htmlx, javascript, RazorCSHarp, RazorVisualBasic";
        private string _supportedRegionContenTypes = "CSharp, Basic, RazorCSharp, RazorVisualBasic";
        private bool _underlineComments;
        private int _userComment1FontSize = DefaulttFontSize;
        private string _userComment1Symbols = "Hack:,Todo:";
        private int _userComment2FontSize = DefaulttFontSize;
        private string _userComment2Symbols = "Note:";
        private int _userComment3FontSize = DefaulttFontSize;
        private string _userComment3Symbols = "Resharper";

        #endregion Fields

        #region Regions General

        [Category( "Regions General                                    " + Document )]
        [Description(
            "Comma separated, case-insensitive list of content types. You can add more. Default: 'CSharp, Basic, RazorCSHarp, RazorVisualBasic'."
            )]
        [DisplayName( @"Supported Region ContenTypes" )]
        public string SupportedRegionContenTypes {
            get { return _supportedRegionContenTypes; }
            set { _supportedRegionContenTypes = value; }
        }

        [Category( "Regions General                                    " + Restart )]
        [Description( "Borderthickness in pixels." )]
        [DisplayName( @"Borderthickness" )]
        public int BorderThickness {
            get { return _borderThickness; }
            set { _borderThickness = value; }
        }

        #endregion Regions General

        #region User comments

        [Category( "User Comment #1                                 " + Document )]
        [Description( "Comma separated, case-insensitive list of symbols to match for. Default: 'Hack:, Todo:'." )]
        [DisplayName( @"List of symbols" )]
        public string UserComment1Symbols {
            get { return _userComment1Symbols; }
            set { _userComment1Symbols = value; }
        }

        [Category( "User Comment #2                                 " + Document )]
        [Description( "Comma separated, case-insensitive list of symbols to match for. Default: 'Note:'." )]
        [DisplayName( @"List of symbols" )]
        public string UserComment2Symbols {
            get { return _userComment2Symbols; }
            set { _userComment2Symbols = value; }
        }

        [Category( "User comment #3                                 " + Document )]
        [Description( "Comma separated, case-insensitive list of symbols to match for. Default: 'Resharper'." )]
        [DisplayName( @"List of symbols" )]
        public string UserComment3Symbols {
            get { return _userComment3Symbols; }
            set { _userComment3Symbols = value; }
        }

        #endregion User Comment

        #region Font Sizes

        [Category( "Font Sizes                                               " + Restart )]
        [Description( "User comment #1 font size." )]
        [DisplayName( @"User comment #1" )]
        public int UserComment1FontSize {
            get { return _userComment1FontSize; }
            set { _userComment1FontSize = value; }
        }

        [Category( "Font Sizes                                               " + Restart )]
        [Description( "User comment #2 font size." )]
        [DisplayName( @"User comment #2" )]
        public int UserComment2FontSize {
            get { return _userComment2FontSize; }
            set { _userComment2FontSize = value; }
        }

        [Category( "Font Sizes                                               " + Restart )]
        [Description( "User comment #3 font size." )]
        [DisplayName( @"User comment #3" )]
        public int UserComment3FontSize {
            get { return _userComment3FontSize; }
            set { _userComment3FontSize = value; }
        }

        [Category( "Font Sizes                                               " + Restart )]
        [Description( "Standard code comment font size." )]
        [DisplayName( @"Standard comment" )]
        public double? DefaultCommentFontSize {
            get { return _defaultCommentFontSize; }
            set { _defaultCommentFontSize = value; }
        }

        [Category( "Font Sizes                                               " + Restart )]
        [Description( "//* comment font size." )]
        [DisplayName( @"Important comment" )]
        public double? ImportantCommentFontSize {
            get { return _importantCommentFontSize; }
            set { _importantCommentFontSize = value; }
        }

        [Category( "Font Sizes                                               " + Restart )]
        [Description( "//? comment font size." )]
        [DisplayName( @"Question comment" )]
        public double? QuestionCommentFontSize {
            get { return _questionCommentFontSize; }
            set { _questionCommentFontSize = value; }
        }

        [Category( "Font Sizes                                               " + Restart )]
        [Description( "//+ comment font size." )]
        [DisplayName( @"Big comment" )]
        public double? BigCommentFontSize {
            get { return _bigCommentFontSize; }
            set { _bigCommentFontSize = value; }
        }

        [Category( "Font Sizes                                               " + Restart )]
        [Description( "//x comment font size." )]
        [DisplayName( @"Deleted comment" )]
        public double? DeletedCommentFontSize {
            get { return _deletedCommentFontSize; }
            set { _deletedCommentFontSize = value; }
        }

        [Category( "Font Sizes                                               " + Restart )]
        [Description( "//- comment font size." )]
        [DisplayName( @"Small comment" )]
        public double? SmallCommentFontSize {
            get { return _smallCommentFontSize; }
            set { _smallCommentFontSize = value; }
        }

        #endregion

        #region Comments General

        [Category( "Comments General                              " + Document )]
        [Description(
            "Comma separated, case-insensitive list of content types. You can add more. Default: 'plaintext, CSharp, Basic, XML, HTML, htmlx, javascript, RazorCSharp, RazorVisualBasic'."
            )]
        [DisplayName( @"Supported Comment ContenTypes" )]
        public string SupportedCommentContenTypes {
            get { return _supportedCommentContenTypes; }
            set { _supportedCommentContenTypes = value; }
        }

        [Category( "Comments General                              " + Restart )]
        [Description( "The font to use for comments. Default: 'Segoe UI'." )]
        [DisplayName( @"Font Name" )]
        public string CommentsFontFamilyName {
            get { return _commentsFontFamilyName; }
            set { _commentsFontFamilyName = value; }
        }

        [Category( "Comments General                              " + Restart )]
        [Description( "Enable or disable underline." )]
        [DisplayName( @"Underline Comments enabled" )]
        // ReSharper disable once ConvertToAutoProperty
        public bool UnderlineComments {
            get { return _underlineComments; }
            set { _underlineComments = value; }
        }

        [Category( "Comments General                              " + Restart )]
        [Description( "Enable or disable italic." )]
        [DisplayName( @"Italic Comments enabled" )]
        public bool ItalicComments {
            get { return _italicComments; }
            set { _italicComments = value; }
        }

        #endregion Comments

        #region Modules

        [Category( "Modules                                                  " + Document )]
        [Description( "Enable or disable region module." )]
        [DisplayName( @"Regions enabled" )]
        public bool RegionHighlighting {
            get { return _regionHighlighting; }
            set { _regionHighlighting = value; }
        }

        [Category( "Modules                                                  " + Document )]
        [Description( "Enable or disable standard comment module." )]
        [DisplayName( @"Standard comments enabled" )]
        public bool StandardCommentHighlighting {
            get { return _standardCommentHighlighting; }
            set { _standardCommentHighlighting = value; }
        }

        [Category( "Modules                                                  " + Document )]
        [Description( "Enable or disable HTML comment module." )]
        [DisplayName( @"Html comments enabled" )]
        public bool HtmlCommentHighlighting {
            get { return _htmlCommentHighlighting; }
            set { _htmlCommentHighlighting = value; }
        }

        [Category( "Modules                                                  " + Document )]
        [Description( "Enable or disable Razor comment module." )]
        [DisplayName( @"Razor comments enabled" )]
        public bool RazorCommentHighlighting {
            get { return _razorCommentHighlighting; }
            set { _razorCommentHighlighting = value; }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Handles "Activate" messages from the Visual Studio environment.
        /// </summary>
        /// <devdoc>
        ///     This method is called when Visual Studio wants to activate this page.
        /// </devdoc>
        /// <remarks>If the Cancel property of the event is set to true, the page is not activated.</remarks>
        protected override void OnActivate( CancelEventArgs e ) {
            //            DialogResult result = WinFormsHelper.ShowMessageBox(Resources.MessageOnActivateEntered, Resources.MessageOnActivateEntered, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            //
            //            if (result == DialogResult.Cancel)
            //            {
            //                Trace.WriteLine(string.Format(CultureInfo.CurrentCulture, "Cancelled the OnActivate event"));
            //                e.Cancel = true;
            //            }

            base.OnActivate( e );
        }

        /// <summary>
        ///     Handles "Close" messages from the Visual Studio environment.
        /// </summary>
        /// <devdoc>
        ///     This event is raised when the page is closed.
        /// </devdoc>
        protected override void OnClosed( EventArgs e ) {
            //            WinFormsHelper.ShowMessageBox(Resources.MessageOnClosed);
        }

        /// <summary>
        ///     Handles "Deactive" messages from the Visual Studio environment.
        /// </summary>
        /// <devdoc>
        ///     This method is called when VS wants to deactivate this
        ///     page.  If true is set for the Cancel property of the event,
        ///     the page is not deactivated.
        /// </devdoc>
        /// <remarks>
        ///     A "Deactive" message is sent when a dialog page's user interface
        ///     window loses focus or is minimized but is not closed.
        /// </remarks>
        protected override void OnDeactivate( CancelEventArgs e ) {
            //            DialogResult result = WinFormsHelper.ShowMessageBox(Resources.MessageOnDeactivateEntered, Resources.MessageOnDeactivateEntered, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            //
            //            if (result == DialogResult.Cancel)
            //            {
            //                Trace.WriteLine(string.Format(CultureInfo.CurrentCulture, "Cancelled the OnDeactivate event"));
            //                e.Cancel = true;
            //            }
        }

        #endregion Event Handlers

        /// <summary>
        ///     Handles Apply messages from the Visual Studio environment.
        /// </summary>
        /// <devdoc>
        ///     !This method is called when VS wants to save the user's changes, then the dialog is dismissed.
        /// </devdoc>
        protected override void OnApply( PageApplyEventArgs e ) {
            // Todo: Add more value checks! 

            if ( !IsValidFont( e ) )
                return;

            Options.RefreshCachedValues();

            base.OnApply( e );

            #region Unused for now

            //				if ( result == DialogResult.Cancel ) {
            //					Trace.WriteLine( string.Format( CultureInfo.CurrentCulture, "Cancelled the OnApply event" ) );
            //					e.ApplyBehavior = ApplyKind.Cancel;
            //				} else base.OnApply( e );
            //			WinFormsHelper.ShowMessageBox( Resources.MessageOnApply );

            #endregion
        }


        private static bool IsValidFont( PageApplyEventArgs eventArgs ) {
            string fontname = Options.GetValue( o => o.CommentsFontFamilyName );

            Contract.Assert( fontname != null );

            bool fontIsValid = Options.FontFamilyExists( fontname );

            if ( !fontIsValid ) {
                MessageBoxHelper.ShowMessageBox( string.Format( "Font not found: '{0}'", fontname ), MessageBoxButtons.OK );
                eventArgs.ApplyBehavior = ApplyKind.Cancel;
                return false;
            }

            return true;
        }
    }
}
