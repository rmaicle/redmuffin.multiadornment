﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle( "redmuffin.MultiAdornment" )]
[assembly:
    AssemblyDescription(
        "redmuffin.MultiAdornment Extension for Visual Studio 2012 & 2013 places visual adornments around regions and preprocessor directives and highlights special code comments in C#, VB.Net and javaScript even HTML and Razor comments."
        )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]
[assembly: CLSCompliant( false )]
[assembly: NeutralResourcesLanguage( "en-US" )]

//[assembly: InternalsVisibleTo("redmuffin.MultiAdornment_IntegrationTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100b1a63599d3c51c84881568db2dfcd6b232c5fda0ee1db0ee4c2825da023838e5097b75ebb82d1c47b3cd5299409eefb40bf59a89fb3b516e26c8743b9e01367e28195859b74b5fc8db997abd7147a5e80f89277348074dbc1995a4cc761790e0bee5e6cad33d32e052b8fd74b119e8ce9e7fda94b8786fafaa33182a3bb68d9f")]
[assembly: InternalsVisibleTo( "redmuffin.MultiAdornment.UnitTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100b1a63599d3c51c84881568db2dfcd6b232c5fda0ee1db0ee4c2825da023838e5097b75ebb82d1c47b3cd5299409eefb40bf59a89fb3b516e26c8743b9e01367e28195859b74b5fc8db997abd7147a5e80f89277348074dbc1995a4cc761790e0bee5e6cad33d32e052b8fd74b119e8ce9e7fda94b8786fafaa33182a3bb68d9f" )]
