Imports Microsoft.VisualBasic

#Region "MyRegion"

Sub Main()
    ' Test
    ' ? Test
    ' ! Test
    ' x test
    ' - test
    ' + test
    ' Hack: test
    ' Note: test
    ' ReSharper
    ''' <summary>
    ''' !Valuable Information goes here
    ''' </summary>
End Sub

#End Region

#If True Then

'Basic'

#Else

#End If


Sub MySub1()
    Dim myvar As String = " ' + my text goes here and is not recognized as comment '"    ' ! but this should be recognized
    Debug.Print(String.Format("An exception occurred while adding the culture '{0}' to the list of supported cultures. {1}", CultureName, ex.ToString))
End Sub
