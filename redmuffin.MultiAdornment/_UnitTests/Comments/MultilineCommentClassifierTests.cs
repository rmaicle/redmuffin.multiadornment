﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using FluentAssertions;
using Jwc.AutoFixture.Xunit;
using redmuffin.MultiAdornment.Classifiers.CommentClassifiers;
using Xunit.Extensions;

namespace redmuffin.MultiAdornment.UnitTests.Comments
{
    /// <summary>
    ///     The unit tests are very compact and not 100% easy to debug but cover a lot of ground in a simple way
    /// </summary>
    public class MultilineCommentClassifierTests : CommentClassifierTestsBase
    {
        #region Multiline Helpermethods

        protected static void TestComment( string comment, string keyword, string prefix, string suffix ) {
            string text = prefix + comment + suffix;

            Logging.Logger.Trace( "'{0}'\r\n\tprefix:\t'{1}'\r\n\tsuffix:\t'{2}'", text, prefix, suffix );

            text = text + Environment.NewLine;
            List<Group> groups = Multiline( text, keyword );

            groups.Count.ShouldBeEquivalentTo( 1, "one match should be found" );
            Logging.Logger.Trace( "\tfound:\t'{0}'", groups.Single().Value );
            groups.Single().Index.ShouldBeEquivalentTo( prefix.Length, "the start of the match should be after the 'prefix'" );
            groups.Single().Value.Length.ShouldBeEquivalentTo( comment.Length, "the length should be as long as the 'comment'" );
            groups.Single().Value.Should().NotContain( @"http://", "URL's should be excluded" );
        }

        private static void TestMultilineComments( string keyword, string prefix, string suffix ) {
            string regExKeyword = keyword.Length > 1 ? "(" + keyword.ToLower() + ")" : "[" + keyword + "]";

            TestComment( @"/* " + keyword + " a simple Multiline comment */", regExKeyword, prefix, suffix );
            TestComment( @"/*" + keyword + "a simple Multiline comment*/", regExKeyword, prefix, suffix );
        }

        internal static List<Group> Multiline( string text, string keyword ) {
            return FindMatches( keyword, MultilineCommentClassifier.Start, MultilineCommentClassifier.End, text );
        }

        #endregion

        [Spec, PropertyData( "SurroundingData" )]
        public void ImportantMultilineComments1( string prefix, string suffix ) {
            TestMultilineComments( @"!", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void BigMultilineComments1( string prefix, string suffix ) {
            TestMultilineComments( @"+", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void QuestionMultilineComments1( string prefix, string suffix ) {
            TestMultilineComments( @"?", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void DeletedMultilineComments1( string prefix, string suffix ) {
            TestMultilineComments( @"x", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void SmallMultilineComments1( string prefix, string suffix ) {
            TestMultilineComments( @"-", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void UserMultilineComments1( string prefix, string suffix ) {
            TestMultilineComments( @"hAck:", prefix, suffix );
        }
    }
}
