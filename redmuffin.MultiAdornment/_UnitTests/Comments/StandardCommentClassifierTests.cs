﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using FluentAssertions;
using Jwc.AutoFixture.Xunit;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Classification.Fakes;
using Microsoft.VisualStudio.Text.Fakes;
using Microsoft.VisualStudio.Utilities;
using Microsoft.VisualStudio.Utilities.Fakes;
using redmuffin.MultiAdornment.Classifiers;
using redmuffin.MultiAdornment.Classifiers.CommentClassifiers;
using Xunit.Extensions;

namespace redmuffin.MultiAdornment.UnitTests.Comments
{
    /// <summary>
    ///     The unit tests are very compact and not 100% easy to debug but cover a lot of ground in a simple way
    /// </summary>
    public class StandardCommentClassifierTests : CommentClassifierTestsBase
    {
        #region StandardComment Helpermethods

        private static void TestWithSnapshotSpan( string comment, string prefix, string suffix, IContentType contentType ) {
            string text = prefix + comment + suffix;
            Logging.Logger.Trace( string.Format( @"{0}", text ) );
            text = text + Environment.NewLine;

            SnapshotSpan snapshotSpan = CreateSnapshotSpan( text, contentType );
            Regex regEx = CommentHighlighter.CreateRegEx( StandardCommentClassifier.Start, StandardCommentClassifier.End, String.Empty );

            // Act
            IEnumerable<ClassificationSpan> spans =
                CommentHighlighter.FindMatches( snapshotSpan, text, regEx, new StubIClassificationType() ).ToList();

            // Assert
            spans.Count().ShouldBeEquivalentTo( 1, "should find 1 match" );
            spans.First().Span.Length.ShouldBeEquivalentTo( text.Length - prefix.Length, "should be: text.Length - prefix.Length" );
        }

        private static StubIContentType ContentType( string typeName ) {
            return new StubIContentType
                   {
                       BaseTypesGet = () => new List<IContentType>(),
                       IsOfTypeString = t => true,
                       DisplayNameGet = () => typeName,
                       TypeNameGet = () => typeName,
                   };
        }

        private static SnapshotSpan CreateSnapshotSpan( string text, IContentType contentType ) {
            StubITextSnapshot stubITextSnapshot = ConfigureStubITextSnapshot( text, text.Length, contentType );
            var snapshotSpan = new SnapshotSpan( stubITextSnapshot, 0, text.Length );
            return snapshotSpan;
        }

        private static StubITextSnapshot ConfigureStubITextSnapshot( string text, int length, IContentType contentType ) {
            var stubITextSnapshot =
                new StubITextSnapshot
                {
                    GetText = () => text,
                    LineCountGet = () => 1,
                    LengthGet = () => length,
                    ContentTypeGet = () => contentType,
                };

            return stubITextSnapshot;
        }

        protected static void TestComment( string comment, string keyword, string prefix, string suffix ) {
            string text = prefix + comment + suffix;
            Logging.Logger.Trace( "'{0}'\r\n\tprefix:\t'{1}'\r\n\tsuffix:\t'{2}'", text, prefix, suffix );
            text = text + Environment.NewLine;

            // Act
            List<Group> groups = StandardComment( text, keyword );

            // Assert
            groups.Count.ShouldBeEquivalentTo( 1, "one match should be found" );
            Logging.Logger.Trace( string.Format( "\tfound:\t'{0}'", groups.Single().Value ) );
            groups.Single().Index.ShouldBeEquivalentTo( prefix.Length, "the start of the match should be after the 'prefix'" );
            groups.Single()
                .Value.Length.ShouldBeEquivalentTo( text.Length - prefix.Length, "the length should be as long as the ('text' - 'prefix')" );
        }

        private static void TestStandardComments( string keyword, string prefix, string suffix ) {
            string regExKeyword = keyword.Length > 1 ? "(" + keyword.ToLower() + ")" : "[" + keyword + "]";

            TestComment( @"// " + keyword + " a simple comment", regExKeyword, prefix, suffix );
            TestComment( @"//" + keyword + "a simple comment", regExKeyword, prefix, suffix );
            TestComment( @"/// " + keyword + " a simple comment", regExKeyword, prefix, suffix );
            TestComment( @"///" + keyword + "a simple comment", regExKeyword, prefix, suffix );
        }

        internal static List<Group> StandardComment( string text, string keyword ) {
            return FindMatches( keyword, StandardCommentClassifier.Start, StandardCommentClassifier.End, text );
        }

        #endregion

        [Spec, PropertyData( "SurroundingData" )]
        public void ImportantCommentsReal1( string prefix, string suffix ) {
            const string comment = @"/// ! some important information within a standard VB.net comment";
            TestWithSnapshotSpan(
                comment,
                prefix,
                suffix,
                new StubIContentType
                {
                    TypeNameGet = () => "CSharp",
                } );
        }

        [Spec]
        public void CommentsInQuotesShouldBeIgnored1() {
            // Todo: find a way to put this constraint into regex. Is this even possible?
            string text = @"var text = ""a textblock and // this should not be recognized as comment""";

            Logging.Logger.Trace( string.Format( @"'{0}'\r\n\", text ) );

            text = text + Environment.NewLine;
            List<Group> groups = StandardComment( text, String.Empty );

            groups.Count.ShouldBeEquivalentTo( 0, "comments in strings should be ignored" );
        }

        [Spec]
        public void CommentsInQuotesShouldBeIgnored2() {
            // Todo: find a way to put this constraint into regex. Is this even possible?
            string text = @"    if( mystring.Contains(""some text with slashes // should not be recognized as comment"") {";

            Logging.Logger.Trace( string.Format( @"'{0}'\r\n\", text ) );

            text = text + Environment.NewLine;
            List<Group> groups = StandardComment( text, String.Empty );

            groups.Count.ShouldBeEquivalentTo( 0, "comments in strings should be ignored" );
        }

        [Spec]
        public void UrlIsNoComment( string someString ) {
            string text = "http://mydomain.com";

            Logging.Logger.Trace( string.Format( @"'{0}'\r\n\", text ) );

            text = text + Environment.NewLine;
            List<Group> groups = StandardComment( text, String.Empty );

            groups.Count.ShouldBeEquivalentTo( 0, "no url should be mistaken as a comment" );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void ImportantComments1( string prefix, string suffix ) {
            TestStandardComments( @"!", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void BigComments1( string prefix, string suffix ) {
            TestStandardComments( @"+", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void QuestionComments1( string prefix, string suffix ) {
            TestStandardComments( @"?", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void DeletedComments1( string prefix, string suffix ) {
            TestStandardComments( @"x", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void SmallComments1( string prefix, string suffix ) {
            TestStandardComments( @"-", prefix, suffix );
        }

        [Spec, PropertyData( "SurroundingData" )]
        public void UserComments1( string prefix, string suffix ) {
            TestStandardComments( @"hAck:", prefix, suffix );
        }
    }
}
