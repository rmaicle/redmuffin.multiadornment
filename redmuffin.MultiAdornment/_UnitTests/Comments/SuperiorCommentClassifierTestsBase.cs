﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Classification.Fakes;
using Microsoft.VisualStudio.Text.Fakes;
using Microsoft.VisualStudio.Utilities;
using Microsoft.VisualStudio.Utilities.Fakes;
using NLog;
using redmuffin.MultiAdornment.Classifiers;
using Xunit.Extensions;

namespace redmuffin.MultiAdornment.UnitTests.Comments
{
    public class SuperiorCommentClassifierTestsBase
    {
        private const string ExampleContentTypeName = "csharp";

        #region Logger

        private static Logger _logger;

        private static Logger Logger {
            get {
                if ( _logger != null ) return _logger;
                if ( LogManager.Configuration == null ) LogManager.Configuration = Logging.CreateConfiguration();
                return _logger = LogManager.GetCurrentClassLogger();
            }
        }

        #endregion

        protected readonly StubIClassificationType BigClassificationType =
            new StubIClassificationType { ClassificationGet = () => BigStyle.ClassName };

        protected readonly StubIClassificationType DeletedClassificationType =
            new StubIClassificationType { ClassificationGet = () => DeletedStyle.ClassName };

        protected readonly StubIClassificationType NoteClassificationType =
            new StubIClassificationType { ClassificationGet = () => NoteStyle.ClassName };

        protected readonly StubIClassificationType ImportantClassificationType =
            new StubIClassificationType { ClassificationGet = () => ImportantStyle.ClassName };

        protected readonly StubIClassificationType QuestionClassificationType =
            new StubIClassificationType { ClassificationGet = () => QuestionStyle.ClassName };

        protected readonly StubIClassificationType SmallClassificationType =
            new StubIClassificationType { ClassificationGet = () => SmallStyle.ClassName };

        protected readonly StubIClassificationType StandardClassificationType =
            new StubIClassificationType { ClassificationGet = () => StandardStyle.ClassName };

        protected readonly StubIClassificationType User1ClassificationType =
            new StubIClassificationType { ClassificationGet = () => UserStyle1.ClassName };

        protected readonly StubIClassificationType User2ClassificationType =
            new StubIClassificationType { ClassificationGet = () => UserStyle2.ClassName };

        protected readonly StubIClassificationType User3ClassificationType =
            new StubIClassificationType { ClassificationGet = () => UserStyle3.ClassName };

        public SuperiorCommentClassifierTestsBase() {
            Contract.ContractFailed += ( sender, e ) => {
                e.SetHandled();
                e.SetUnwind(); // cause code to abort after event

                Assert.Fail( "{0}\r\n\r\n{1}", e.Message, e.FailureKind.ToString() );
            };
        }

        /// <summary>
        ///     Creates a STUB <see cref="SnapshotSpan" />.
        /// </summary>
        protected SnapshotSpan CreateStubSnapshotSpan(
            string text,
            string contentType = ExampleContentTypeName ) {
            StubITextSnapshot textSnapshot = CreateStubITextSnapshot( text, text.Length, contentType );
            return new SnapshotSpan( textSnapshot, 0, text.Length );
        }

        /// <summary>
        ///     Creates a STUB <see cref="IContentType" />
        /// </summary>
        private StubIContentType CreateStubContentType( string typeName ) {
            return new StubIContentType
                   {
                       DisplayNameGet = () => typeName,
                       TypeNameGet = () => typeName,
                   };
        }

        /// <summary>
        ///     Creates a STUB <see cref="ITextSnapshot" />
        /// </summary>
        private StubITextSnapshot CreateStubITextSnapshot( string text, int length, string contentType ) {
            return new StubITextSnapshot
                   {
                       GetTextInt32Int32 = ( s, e ) => text,
                       GetTextSpan = s => text,
                       GetText = () => text,
                       LineCountGet = () => 1,
                       LengthGet = () => length,
                       ContentTypeGet = () => CreateStubContentType( contentType ),
                   };
        }

        /// <summary>
        ///     Create standard comment options.
        /// </summary>
        protected static CommentOptions StandardCommentOptions() {
            return new CommentOptions(
                true,
                new[] { ExampleContentTypeName },
                new HashSet<string>( new[] { "hack", "todo" } ),
                new HashSet<string>( new[] { "note" } ),
                new HashSet<string>( new[] { "resharper" } )
                );
        }

        /// <summary>
        ///     Build the text from prefix, comment, suffix and <see cref="Environment.NewLine" />
        /// </summary>
        protected string BuildText( string prefix, string suffix, string comment ) {
            string text = prefix + comment + suffix;
            Logger.Debug( string.Format( "Created Test-Text:\t...{0}...", text ) );
            text = text + Environment.NewLine;
            return text;
        }

        /// <summary>
        ///     Create a STUB <see cref="IClassificationTypeRegistryService" />.
        /// </summary>
        protected StubIClassificationTypeRegistryService CreateRegistry() {
            return new StubIClassificationTypeRegistryService
                   {
                       GetClassificationTypeString = s => {
                           if ( s == NoteStyle.ClassName ) return NoteClassificationType;
                           if ( s == ImportantStyle.ClassName ) return ImportantClassificationType;
                           if ( s == QuestionStyle.ClassName ) return QuestionClassificationType;
                           if ( s == BigStyle.ClassName ) return BigClassificationType;
                           if ( s == SmallStyle.ClassName ) return SmallClassificationType;
                           if ( s == DeletedStyle.ClassName ) return DeletedClassificationType;
                           if ( s == StandardStyle.ClassName ) return StandardClassificationType;
                           if ( s == UserStyle1.ClassName ) return User1ClassificationType;
                           if ( s == UserStyle2.ClassName ) return User2ClassificationType;
                           if ( s == UserStyle3.ClassName ) return User3ClassificationType;
                           throw new InvalidOperationException( "ClassificationType not supported: " + s );
                       }
                   };
        }

        /// <summary>
        ///     A list of prefix and suffix combinations to surround the comments with.
        /// </summary>
        protected class SuccessfulCustomDataAttribute : DataAttribute
        {
            public override IEnumerable<object[]> GetData( MethodInfo methodUnderTest, Type[] parameterTypes ) {
                // ! Html special cases
                yield return new object[] { "  ", @"  -->     " };
                yield return new object[] { " -->  ", @"       " };
                yield return new object[] { "   ", @"    <!--   " };

                // ! Razor special cases
                yield return new object[] { "  ", @"  *@     " };
                yield return new object[] { " *@  ", @"       " };
                yield return new object[] { "   ", @"    @*   " };

                // ! multi-line special cases
                yield return new object[] { "  ", @"  */     " };
                yield return new object[] { " */  ", @"       " };
                yield return new object[] { "   ", @"    /*   " };

                yield return new object[] { String.Empty, String.Empty };
                yield return new object[] { "        ", @"       " };
                yield return new object[] { "something before", String.Empty };
                yield return new object[] { String.Empty, @"something after" };
                yield return new object[] { "something before", @"AND something after" };

                // ! Visual Basic comment special cases
                yield return new object[] { @"  Debug.Print(String.Format(""An exception occurred while adding the culture '{0}' to the list of supported cultures. {1}"", CultureName, ex.ToString))" };
                yield return new object[] { @"    Dim query As New WqlEventQuery(String.Format(""SELECT * FROM RegistryTreeChangeEvent WHERE Hive = 'HKEY_USERS' AND RootPath = '{0}'"", key))" };
                yield return new object[] { @"MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, ""Couldn't add panel"" & vbNewLine & ex.Message)" };
                yield return new object[] { @"Dim commentCharacters() As Char = New Char() {""#"", "";"", ""'""}" };
                yield return new object[] { @"_sqlQuery = New SqlCommand(""INSERT INTO tblRoot (Name, Export, Protected, ConfVersion) VALUES('"" & PrepareValueForDB(tN.Text) & ""', 0, '"" & strProtected & ""',"" & App.Info.Connections.ConnectionFileVersion.ToString(CultureInfo.InvariantCulture) & "")"", _sqlConnection)        " };

                // ! standard comment url special cases
                yield return new object[] { @"an url before http://beforedomain.com", @"AND something after" };
                yield return new object[] { @"something before", @"AND an url after http://afterdomain.com" };
            }
        }

        protected class UnSuccessfulCustomDataAttribute : DataAttribute
        {
            public override IEnumerable<object[]> GetData( MethodInfo methodUnderTest, Type[] parameterTypes ) {
                // ! Visual Basic comment special cases
                yield return new object[] { @"  Debug.Print(String.Format(""An exception occurred while adding the culture '{0}' to the list of supported cultures. {1}"", CultureName, ex.ToString))" };
                yield return new object[] { @"    Dim query As New WqlEventQuery(String.Format(""SELECT * FROM RegistryTreeChangeEvent WHERE Hive = 'HKEY_USERS' AND RootPath = '{0}'"", key))" };
                yield return new object[] { @"MessageCollector.AddMessage(Messages.MessageClass.ErrorMsg, ""Couldn't add panel"" & vbNewLine & ex.Message)" };
                yield return new object[] { @"Dim commentCharacters() As Char = New Char() {""#"", "";"", ""'""}" };
                yield return new object[] { @"_sqlQuery = New SqlCommand(""INSERT INTO tblRoot (Name, Export, Protected, ConfVersion) VALUES('"" & PrepareValueForDB(tN.Text) & ""', 0, '"" & strProtected & ""',"" & App.Info.Connections.ConnectionFileVersion.ToString(CultureInfo.InvariantCulture) & "")"", _sqlConnection)        " };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
                yield return new object[] { };
            }
        }
    }
}
