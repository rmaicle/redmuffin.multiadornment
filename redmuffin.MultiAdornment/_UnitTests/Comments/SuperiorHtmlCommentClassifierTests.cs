﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Jwc.AutoFixture.Xunit;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Classification.Fakes;
using redmuffin.MultiAdornment.Classifiers;

namespace redmuffin.MultiAdornment.UnitTests.Comments
{
    // ReSharper disable JoinDeclarationAndInitializer
    public class SuperiorHtmlCommentClassifierTests : SuperiorCommentClassifierTestsBase
    {
        private const string HtmlCommentOpening = @"<!--";
        private const string HtmlCommentClosing = @"-->";

        private void TestComment(
            string prefix,
            string suffix,
            string key,
            StubIClassificationType classificationType,
            string opening,
            string closing ) {
            // Arrange
            string text = BuildText( prefix, suffix, opening + @" " + key + " lorem ipsum dolor sit amet " + closing );
            SnapshotSpan snapshotSpan = CreateStubSnapshotSpan( text );
            var sut = new SuperiorHtmlCommentClassifier( CreateRegistry(), StandardCommentOptions() );

            // Act  
            IList<ClassificationSpan> result = sut.GetClassificationSpans( snapshotSpan );

            // Assert
            result.Count().ShouldBeEquivalentTo( 1, "should find 1 match" );
            result.First()
                .Span.Length.ShouldBeEquivalentTo(
                    text.Length - prefix.Length - suffix.Length - Environment.NewLine.Length,
                    "the length should be text.Length - prefix.Length - suffix.Length - Environment.NewLine.Length" );

            IClassificationType type = result.First().ClassificationType;
            string classificationGet = classificationType.ClassificationGet();
            type.Classification.Should().Be( classificationGet );
        }

        [Spec, SuccessfulCustomData]
        public void HtmlComment( string prefix, string suffix ) {
            TestComments( prefix, suffix, HtmlCommentOpening, HtmlCommentClosing );
        }

        private void TestComments( string prefix, string suffix, string opening, string closing ) {
            TestComment( prefix, suffix, ".", NoteClassificationType, opening, closing );
            TestComment( prefix, suffix, "!", ImportantClassificationType, opening, closing );
            TestComment( prefix, suffix, "?", QuestionClassificationType, opening, closing );
            TestComment( prefix, suffix, "+", BigClassificationType, opening, closing );
            TestComment( prefix, suffix, "-", SmallClassificationType, opening, closing );
            TestComment( prefix, suffix, "x", DeletedClassificationType, opening, closing );
            TestComment( prefix, suffix, "X", DeletedClassificationType, opening, closing );
            TestComment( prefix, suffix, "HACK", User1ClassificationType, opening, closing );
            TestComment( prefix, suffix, "todo", User1ClassificationType, opening, closing );
            TestComment( prefix, suffix, "noTE", User2ClassificationType, opening, closing );
            TestComment( prefix, suffix, "ReSharpER", User3ClassificationType, opening, closing );
            TestComment( prefix, suffix, "", StandardClassificationType, opening, closing );
        }
    }
}
