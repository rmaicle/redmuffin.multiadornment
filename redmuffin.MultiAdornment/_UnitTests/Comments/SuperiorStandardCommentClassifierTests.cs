﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Jwc.AutoFixture.Xunit;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Classification.Fakes;
using redmuffin.MultiAdornment.Classifiers;

namespace redmuffin.MultiAdornment.UnitTests.Comments
{
    // ReSharper disable JoinDeclarationAndInitializer
    public class SuperiorStandardCommentClassifierTests : SuperiorCommentClassifierTestsBase
    {
        internal const string StandardCommentOpening = @"//";
        internal const string StandardXmlCommentOpening = @"///";

        private void TestComment( string prefix, string suffix, string key, StubIClassificationType classificationType, string opening ) {
            // Arrange
            string text = BuildText( prefix, suffix, opening + @" " + key + " lorem ipsum dolor sit amet" );
            SnapshotSpan snapshotSpan = CreateStubSnapshotSpan( text );
            var sut = new SuperiorStandardCommentClassifier( CreateRegistry(), StandardCommentOptions() );

            // Act  
            IList<ClassificationSpan> result = sut.GetClassificationSpans( snapshotSpan );

            // Assert
            result.Count().ShouldBeEquivalentTo( 1, "should find 1 match" );
            result.First().Span.Length.ShouldBeEquivalentTo( text.Length - prefix.Length, "the length should be text without prefix" );

            IClassificationType type = result.First().ClassificationType;
            string classificationGet = classificationType.ClassificationGet();
            type.Classification.Should().Be( classificationGet );
        }

        [Spec, SuccessfulCustomData]
        public void RegularStandardComment( string prefix, string suffix ) {
            TestComments( prefix, suffix, StandardCommentOpening );
        }

        [Spec, SuccessfulCustomData]
        public void XmlStandardComment( string prefix, string suffix ) {
            TestComments( prefix, suffix, StandardXmlCommentOpening );
        }

        private void TestComments( string prefix, string suffix, string opening ) {
            TestComment( prefix, suffix, ".", NoteClassificationType, opening );
            TestComment( prefix, suffix, "!", ImportantClassificationType, opening );
            TestComment( prefix, suffix, "?", QuestionClassificationType, opening );
            TestComment( prefix, suffix, "+", BigClassificationType, opening );
            TestComment( prefix, suffix, "-", SmallClassificationType, opening );
            TestComment( prefix, suffix, "x", DeletedClassificationType, opening );
            TestComment( prefix, suffix, "X", DeletedClassificationType, opening );
            TestComment( prefix, suffix, "HACK", User1ClassificationType, opening );
            TestComment( prefix, suffix, "todo", User1ClassificationType, opening );
            TestComment( prefix, suffix, "noTE", User2ClassificationType, opening );
            TestComment( prefix, suffix, "ReSharpER", User3ClassificationType, opening );
            TestComment( prefix, suffix, "", StandardClassificationType, opening );
        }
    }
}
