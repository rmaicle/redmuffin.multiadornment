﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Jwc.AutoFixture.Xunit;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Classification.Fakes;
using redmuffin.MultiAdornment.Classifiers;

namespace redmuffin.MultiAdornment.UnitTests.Comments
{
    // ReSharper disable JoinDeclarationAndInitializer
    public class SuperiorVisualBasicCommentClassifierTests : SuperiorCommentClassifierTestsBase
    {
        private const string VisualBasicCommentOpening = @"'";
        private const string VisualBasicXmlCommentOpening = @"'''";

        private void TestComment(
            string prefix,
            string suffix,
            string key,
            StubIClassificationType classificationType,
            string opening,
            int matchCount ) {
            // Arrange
            string text = BuildText( prefix, suffix, opening + @" " + key + " lorem ipsum dolor sit amet" );

            IList<ClassificationSpan> result = SystemUnderTest( text );

            // Assert
            result.Count().ShouldBeEquivalentTo( matchCount, "should find " + matchCount + " match(es)" );
            if ( matchCount > 0 ) {
                result.First().Span.Length.ShouldBeEquivalentTo( text.Length - prefix.Length, "the length should be text without prefix" );

                IClassificationType type = result.First().ClassificationType;
                string classificationGet = classificationType.ClassificationGet();
                type.Classification.Should().Be( classificationGet );
            }
        }

        private IList<ClassificationSpan> SystemUnderTest( string text ) {
            SnapshotSpan snapshotSpan = CreateStubSnapshotSpan( text );
            var sut = new SuperiorVisualBasicCommentClassifier( CreateRegistry(), StandardCommentOptions() );

            // Act  
            IList<ClassificationSpan> result = sut.GetClassificationSpans( snapshotSpan );
            return result;
        }

        [Spec, SuccessfulCustomData]
        public void SuccessfulRegularVisualBasicComment( string prefix, string suffix ) {
            TestComments( prefix, suffix, VisualBasicCommentOpening );
        }

        [Spec, SuccessfulCustomData]
        public void SuccessfulXmlVisualBasicComment( string prefix, string suffix ) {
            TestComments( prefix, suffix, VisualBasicXmlCommentOpening );
        }

        private void TestComments( string prefix, string suffix, string opening, int matchCount = 1 ) {
            TestComment( prefix, suffix, ".", NoteClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "!", ImportantClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "?", QuestionClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "+", BigClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "-", SmallClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "x", DeletedClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "X", DeletedClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "HACK", User1ClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "todo", User1ClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "NOte:", User2ClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "ReSharpER", User3ClassificationType, opening, matchCount );
            TestComment( prefix, suffix, "", StandardClassificationType, opening, matchCount );
        }
    }
}
