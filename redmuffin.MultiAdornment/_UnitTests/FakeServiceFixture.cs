﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Jwc.AutoFixture.Xunit;
using Moq;
using Ploeh.AutoFixture;
using Xunit;
using Xunit.Extensions;

namespace redmuffin.MultiAdornment.UnitTests
{
    // ! Example
    public class FakeServiceFixture : ICustomization
    {
        public void Customize( IFixture fixture ) {
            fixture.Inject<IService>( new FakeService() );
        }
    }

    public class FakeService : IService {}

    public interface IService {}

    // ! Example
    internal class ExampleCustomDataAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData( MethodInfo methodUnderTest, Type[] parameterTypes ) {
            yield return new object[] { "anonymous", 123 };
        }
    }

    /// <summary>
    ///     https://github.com/jwChung/AutoFixture.Contrib/blob/master/test/AutoFixture.Contrib.UnitTest/Scenario.cs
    /// </summary>
    /// <remarks>
    ///     ! a few, short code examples for Autofixture.Contrib
    /// </remarks>
    public class Examples
    {
        [Spec] // ! Example
        public void SpecCanCreateMockedSpecimenUsingMoqFrameworkButNotReferencingAutoFixtureAutoMoq( IDisposable target ) {
            Assert.IsType<Mock<IDisposable>>( Mock.Get( target ) );
        }

        [Spec( typeof ( StringCustomization ), typeof ( IntegerCustomization ) )] // ! Example
        public void SpecWithCustomizationTypesCanCustomizeFixture( string strValue, int intValue ) {
            Assert.Equal( "anonymous", strValue );
            Assert.Equal( 123, intValue );
        }

        // ! Example
        private class IntegerCustomization : ICustomization
        {
            public void Customize( IFixture fixture ) {
                fixture.Inject( 123 );
            }
        }

        // ! Example
        private class StringCustomization : ICustomization
        {
            public void Customize( IFixture fixture ) {
                fixture.Inject( "anonymous" );
            }
        }
    }
}
