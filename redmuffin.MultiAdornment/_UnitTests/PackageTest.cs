﻿using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VsSDK.UnitTestLibrary;
using redmuffin.MultiAdornment.Core;

namespace redmuffin.MultiAdornment.UnitTests
{
    [TestClass]
    public class PackageTest
    {
        [TestMethod]
        public void CreateInstance() {
            // ReSharper disable once UnusedVariable
            var package = new MultiAdornmentPackage();
        }

        [TestMethod]
        public void IsIVsPackage() {
            var package = new MultiAdornmentPackage() as IVsPackage;
            Assert.IsNotNull( package, "The object does not implement IVsPackage" );
        }

        [TestMethod]
        public void SetSite() {
            // Create the package
            var package = new MultiAdornmentPackage() as IVsPackage;
            Assert.IsNotNull( package, "The object does not implement IVsPackage" );

            // Create a basic service provider
            OleServiceProvider serviceProvider = OleServiceProvider.CreateOleServiceProviderWithBasicServices();

            // Site the package
            Assert.AreEqual( 0, package.SetSite( serviceProvider ), "SetSite did not return S_OK" );

            // Unsite the package
            Assert.AreEqual( 0, package.SetSite( null ), "SetSite(null) did not return S_OK" );
        }
    }
}
