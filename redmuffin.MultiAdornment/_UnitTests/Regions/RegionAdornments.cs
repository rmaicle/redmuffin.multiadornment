﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using FluentAssertions;
using Jwc.AutoFixture.Xunit;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Fakes;
using Microsoft.VisualStudio.Text.Formatting;
using Microsoft.VisualStudio.Utilities;
using Microsoft.VisualStudio.Utilities.Fakes;
using Moq;
using redmuffin.MultiAdornment.Adornments;
using Xunit.Extensions;

namespace redmuffin.MultiAdornment.UnitTests.Regions
{
    // ReSharper disable ConvertToConstant.Local
    public class RegionAdornments
    {
        #region Helpermethods

        private const string ExampleContentTypeName = "csharp";

        public RegionAdornments() {
            Contract.ContractFailed += ( sender, e ) => {
                e.SetHandled();
                e.SetUnwind(); // cause code to abort after event

                Assert.Fail( "{0}\r\n\r\n{1}", e.Message, e.FailureKind.ToString() );
            };
        }

        /// <summary>
        ///     Creates a STUB <see cref="SnapshotSpan" />.
        /// </summary>
        private SnapshotSpan CreateStubSnapshotSpan(
            string text,
            string contentType = ExampleContentTypeName ) {
            StubITextSnapshot textSnapshot = CreateStubITextSnapshot( text, text.Length, contentType );
            return new SnapshotSpan( textSnapshot, 0, text.Length );
        }

        /// <summary>
        ///     Creates a STUB <see cref="IContentType" />
        /// </summary>
        private StubIContentType CreateStubContentType( string typeName ) {
            return new StubIContentType
                   {
                       DisplayNameGet = () => typeName,
                       TypeNameGet = () => typeName,
                   };
        }

        /// <summary>
        ///     Creates a STUB <see cref="ITextSnapshot" />
        /// </summary>
        private StubITextSnapshot CreateStubITextSnapshot( string text, int length, string contentType ) {
            return new StubITextSnapshot
                   {
                       GetTextInt32Int32 = ( s, e ) => text,
                       GetTextSpan = s => text,
                       GetText = () => text,
                       LineCountGet = () => 1,
                       LengthGet = () => length,
                       ContentTypeGet = () => CreateStubContentType( contentType ),
                   };
        }


        private RegionAdornmentCreator RegionAdornmentCreator( string text ) {
            var formatMap = Mock.Of<IEditorFormatMap>();
            var adornmentLayer = Mock.Of<IAdornmentLayer>();

            var animationPath = new PathGeometry();
            var pFigure = new PathFigure();
            pFigure.StartPoint = new Point( 5, 3 );
            var pBezierSegment = new PolyBezierSegment();
            pBezierSegment.Points.Add( new Point( 35, 0 ) );
            pBezierSegment.Points.Add( new Point( 135, 0 ) );
            pBezierSegment.Points.Add( new Point( 160, 100 ) );
            pBezierSegment.Points.Add( new Point( 180, 190 ) );
            pBezierSegment.Points.Add( new Point( 285, 200 ) );
            pBezierSegment.Points.Add( new Point( 310, 100 ) );
            pFigure.Segments.Add( pBezierSegment );
            animationPath.Figures.Add( pFigure );
            animationPath.Freeze();

            var textViewLineCollection = Mock.Of<IWpfTextViewLineCollection>(
                lineCollection =>
                    // ReSharper disable once PossibleUnintendedReferenceComparison
                    lineCollection.GetMarkerGeometry( It.IsAny<SnapshotSpan>() ) == animationPath
                );

            var textView = Mock.Of<IWpfTextView>(
                view =>
                    view.TextSnapshot == (ITextSnapshot)CreateStubITextSnapshot( text, text.Length, ExampleContentTypeName )
                    && view.TextViewLines == textViewLineCollection
                );

            Dictionary<string, List<Brush>> brushes = RegionAdornmentManager.Keywords.ToDictionary(
                keyWord => keyWord,
                keyWord => new List<Brush>
                           {
                               new SolidColorBrush(),
                               new SolidColorBrush(),
                           } );

            var regionOptions = Mock.Of<IRegionOptions>(
                options =>
                    options.Brushes == brushes
                );

            var regionAdornmentManager = Mock.Of<IRegionAdornmentManager>(
                manager =>
                    manager.FormatMap == formatMap &&
                    manager.Layer == adornmentLayer &&
                    manager.RegionOptions == regionOptions &&
                    manager.View == textView
                );

            var regionCreator = new RegionAdornmentCreator( regionAdornmentManager );
            return regionCreator;
        }

        private static ITextViewLine TextViewLine( SnapshotSpan snapshotSpan, string someText ) {
            var line = Mock.Of<ITextViewLine>(
                tvLine =>
                    tvLine.Extent == snapshotSpan &&
                    tvLine.Snapshot.GetText() == someText
                );
            return line;
        }

        #endregion

        [Spec]
        public void RegionAdornmentCreator_Instantiates( IRegionAdornmentManager regionAdornmentManager ) {
            var regionCreator = new RegionAdornmentCreator( regionAdornmentManager );
            regionCreator.Should().NotBeNull();
        }

        [Spec]
        public void RegionAdornmentCreator_CreateVisualsWithoutRegionText_Executes( string someText ) {
            // Arrange
            RegionAdornmentCreator regionCreator = RegionAdornmentCreator( someText );
            SnapshotSpan snapshotSpan = CreateStubSnapshotSpan( someText );
            var line = TextViewLine( snapshotSpan, someText );

            // Act
            regionCreator.CreateVisuals( line );

            // Assert
            Assert.IsTrue( true );
        }

        [Spec]
        // ReSharper disable StringLiteralTypo
        [InlineData( "#region" )]
        [InlineData( "#endregion" )]
        [InlineData( "#else" )]
        [InlineData( "#if" )]
        [InlineData( "#endif" )]
        [InlineData( "#elif" )]
        [InlineData( "#pragma warning disable" )]
        [InlineData( "#pragma warning enable" )]
        [InlineData( "#pragma checksum" )]
        [InlineData( "#line" )]
        [InlineData( "#error" )]
        [InlineData( "#warning" )]
        [InlineData( "#define" )]
        [InlineData( "#undef" )]
        // ReSharper restore StringLiteralTypo
        public void RegionAdornmentCreator_CreateVisualsWithRegionText_Executes( string someText ) {
            // Arrange
            RegionAdornmentCreator regionCreator = RegionAdornmentCreator( someText );
            SnapshotSpan snapshotSpan = CreateStubSnapshotSpan( someText );
            var line = TextViewLine( snapshotSpan, someText );

            // Act
            regionCreator.CreateVisuals( line );

            // Assert
            Assert.IsTrue( true );
            regionCreator.DebugIndentedGeometry.Should().NotBeNull();
            regionCreator.DebugIndentedGeometry.Bounds.Width.Should().BeGreaterThan( 0 );
            regionCreator.DebugIndentedGeometry.Bounds.Height.Should().BeGreaterThan( 0 );

            // TODO: add some Debug values into regionCreator to add more asserts 
        }
    }
}
